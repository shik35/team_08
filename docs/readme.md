### В данной папке содержится документация к проекту

- [Схема базы данных](SberJava.%20Database%20scheme%20for%20Education%20Management%20System.png)
- [Postman коллекция запросов](EMS_JAVA_SCHOOL.postman_collection.json)

В проекте разработана система для ведения образовательного процесса.

В системе доступно:

* создание образовательных курсов с подробным описанием для каждого, включающим полный список лекций курса;
* к каждой лекции курса можно добавить описание и домашнее задание;
* для каждого курса может быть набрано неограниченное количество групп для прохождения обучения;
* каждому студенту любой группы может быть проставлена оценка по пройденному материалу;

Информация о курсах и лекциях общедоступна, остальной функционал закрыт механизмами аутентификации и авторизации.

Доступны следующие типы пользователей, имеющие разные права на доступ к материалам:

* «Администратор»,
* «Преподаватель»,
* «Студент».

Администратору доступен весь функционал системы, который включает создание, редактирование и удаление пользователей, курсов, лекций, групп и оценок.

Преподаватель может редактировать лекции тех курсов, которые он ведет, проставлять оценки по проведенным лекциям тем студентам, которые набраны в группы его курсов и видеть статистику обучения студентов.

Студент имеет права только на просмотр информации о том, в каких группах на каких курсах он обучается, какие оценки получил за пройденный материал получил и какова статистика его обучения.
