package ru.edu.project.frontend;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import ru.edu.project.backend.api.courses.Course;
import ru.edu.project.backend.api.courses.CourseForm;
import ru.edu.project.backend.api.courses.CourseInfo;
import ru.edu.project.backend.api.courses.CourseService;
import ru.edu.project.backend.api.groups.Group;
import ru.edu.project.backend.api.groups.GroupService;
import ru.edu.project.backend.api.lessons.Lesson;
import ru.edu.project.backend.api.lessons.LessonService;
import ru.edu.project.backend.api.students.User;
import ru.edu.project.backend.api.students.UserService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CoursesControllerTest {

    @Mock
    private Model modelMock;

    @Mock
    private Authentication authenticationMock;

    @Mock
    private GrantedAuthority grantedAuthority;

    @Mock
    private CourseService courseService;

    @Mock
    private UserService userService;

    @Mock
    private LessonService lessonService;

    @Mock
    private GroupService groupService;

    @InjectMocks
    private CoursesController controller;

    private Course course = Course
            .builder()
            .id(1L)
            .title("New course")
            .description("description")
            .teacherId(10L)
            .build();

    private Lesson lesson = Lesson
            .builder()
            .id(10L)
            .title("New lesson")
            .description("description")
            .homework("homework")
            .courseId(1L)
            .build();

    private User teacher = User
            .builder()
            .id(10L)
            .name("Ivan")
            .middleName("Ivanovich")
            .surname("Ivanov")
            .role(User.UserRole.Teacher)
            .build();

    private Group group = Group
            .builder()
            .id(30L)
            .title("AT-11")
            .courseId(1L)
            .started(false)
            .build();

    private CourseInfo courseInfo = CourseInfo
            .builder()
            .course(course)
            .teacher(teacher)
            .groups(Arrays.asList(group))
            .lessons(Arrays.asList(lesson))
            .build();

    @Before
    public void beforeMethod() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void index() {
        List<Course> exceptedCoursesList = Arrays.asList(course);
        when(courseService.getCourses()).thenReturn(exceptedCoursesList);

        List<User> exceptedUsersList = Arrays.asList(teacher);
        when(userService.getUsers()).thenReturn(exceptedUsersList);

        List<Group> exceptedGroupsList = Arrays.asList(group);
        when(groupService.getByCourse(course.getId())).thenReturn(exceptedGroupsList);

        courseInfo.setLessons(new ArrayList<>());
        List<CourseInfo> expCourseInfoList = Arrays.asList(courseInfo);

        String viewName = controller.index(modelMock, authenticationMock);
        assertEquals("/course/index", viewName);

        verify(modelMock)
                .addAttribute("courses", expCourseInfoList);
    }

    @Test
    public void createForm() {
        List<User> exceptedUsersList = Arrays.asList(teacher);
        when(userService.getUsers()).thenReturn(exceptedUsersList);

        when(grantedAuthority.getAuthority()).thenReturn(User.UserRole.Administrator.name());
        when(authenticationMock.getAuthorities()).thenReturn((Collection) Arrays.asList(grantedAuthority));
        String viewName = controller.createForm(modelMock, authenticationMock);
        assertEquals("/course/create", viewName);

        verify(modelMock)
                .addAttribute("teachers", exceptedUsersList);
    }

    @Test
    public void createFormToString() {
        CoursesController.CreateForm form = CoursesController.CreateForm
                .builder()
                .id(10L)
                .title("Title")
                .description("Description")
                .teacherId(100L)
                .build();
        assertEquals(
                "CoursesController.CreateForm(id=10, title=Title, description=Description, teacherId=100)",
                form.toString());
    }

    @Test
    public void createFormProcessing() {
        BindingResult bindingResultMock = mock(BindingResult.class);
        when(bindingResultMock.hasErrors()).thenReturn(false);
        CoursesController.CreateForm form = CoursesController.CreateForm
                .builder()
                .title("Title")
                .description("Description")
                .teacherId(100L)
                .build();
        when(courseService.createCourse(any(CourseForm.class))).thenAnswer(
                invocationOnMock -> {
                    CourseForm courseForm = invocationOnMock.getArgument(0, CourseForm.class);
                    assertEquals(form.getTitle(), courseForm.getTitle());
                    assertEquals(form.getDescription(), courseForm.getDescription());
                    assertEquals(form.getTeacherId(), courseForm.getTeacherId());
                    return course;
                });
        when(grantedAuthority.getAuthority()).thenReturn(User.UserRole.Administrator.name());
        when(authenticationMock.getAuthorities()).thenReturn((Collection) Arrays.asList(grantedAuthority));
        String viewName = controller.createFormProcessing(form, bindingResultMock, modelMock, authenticationMock);

        assertEquals("redirect:/course/?created=" + course.getId(), viewName);

        verify(modelMock, times(0)).addAttribute(anyString(), any());
        verify(courseService).createCourse(any(CourseForm.class));
    }

    @Test
    public void createFormProcessingTeacherIdNegativeAndTitleIsBlank() {
        BindingResult bindingResultMock = mock(BindingResult.class);
        when(bindingResultMock.hasErrors()).thenReturn(false);

        CoursesController.CreateForm form = CoursesController.CreateForm
                .builder()
                .id(-1L)
                .title("")
                .description(course.getDescription())
                .teacherId(course.getTeacherId())
                .build();
        when(grantedAuthority.getAuthority()).thenReturn(User.UserRole.Administrator.name());
        when(authenticationMock.getAuthorities()).thenReturn((Collection) Arrays.asList(grantedAuthority));
        String viewName = controller.createFormProcessing(form, bindingResultMock, modelMock, authenticationMock);
        assertEquals("redirect:/course/", viewName);

        verify(modelMock, times(0)).addAttribute(eq("errors"), any());
        verify(courseService, times(0)).saveCourse(any());
    }

    @Test
    public void createFormProcessingError() {
        List<ObjectError> mockErrors = new ArrayList<>();
        BindingResult bindingResultMock = mock(BindingResult.class);
        when(bindingResultMock.hasErrors()).thenReturn(true);
        when(bindingResultMock.getAllErrors()).thenReturn(mockErrors);

        when(grantedAuthority.getAuthority()).thenReturn(User.UserRole.Administrator.name());
        when(authenticationMock.getAuthorities()).thenReturn((Collection) Arrays.asList(grantedAuthority));
        String viewName = controller.createFormProcessing(
                CoursesController.CreateForm
                        .builder()
                        .title("Title")
                        .description("Description")
                        .teacherId(100L)
                        .build(),
                bindingResultMock,
                modelMock,
                authenticationMock);
        assertEquals("/course/create", viewName);
        verify(modelMock).addAttribute("errors", mockErrors);
    }

    @Test
    public void editForm() {
        List<User> exceptedUsersList = Arrays.asList(teacher);
        when(userService.getUsers()).thenReturn(exceptedUsersList);

        List<Group> exceptedGroupsList = Arrays.asList(group);
        when(groupService.getByCourse(course.getId())).thenReturn(exceptedGroupsList);
        when(groupService.getGroups()).thenReturn(exceptedGroupsList);

        when(courseService.getCourse(course.getId())).thenReturn(course);

        when(grantedAuthority.getAuthority()).thenReturn(User.UserRole.Administrator.name());
        when(authenticationMock.getAuthorities()).thenReturn((Collection) Arrays.asList(grantedAuthority));
        when(lessonService.getLessons()).thenReturn(Arrays.asList(lesson));
        String viewName = controller.editForm(modelMock, course.getId(), authenticationMock);
        assertEquals("/course/edit", viewName);

        verify(modelMock)
                .addAttribute("course", courseInfo);

        verify(modelMock)
                .addAttribute("teachers", exceptedUsersList);

        verify(modelMock)
                .addAttribute("groups", exceptedGroupsList);
    }

    @Test
    public void saveChangedCourse() {
        BindingResult bindingResultMock = mock(BindingResult.class);
        when(bindingResultMock.hasErrors()).thenReturn(false);
        CoursesController.CreateForm form = CoursesController.CreateForm
                .builder()
                .id(course.getId())
                .title(course.getTitle())
                .description(course.getDescription())
                .teacherId(course.getTeacherId())
                .build();
        when(grantedAuthority.getAuthority()).thenReturn(User.UserRole.Administrator.name());
        when(authenticationMock.getAuthorities()).thenReturn((Collection) Arrays.asList(grantedAuthority));
        when(lessonService.getLessons()).thenReturn(Arrays.asList(lesson));
        when(courseService.saveCourse(any(CourseForm.class))).thenAnswer(
                invocationOnMock -> {
                    CourseForm courseForm = invocationOnMock.getArgument(0, CourseForm.class);
                    assertEquals(form.getId(), courseForm.getId());
                    assertEquals(form.getTitle(), courseForm.getTitle());
                    assertEquals(form.getDescription(), courseForm.getDescription());
                    assertEquals(form.getTeacherId(), courseForm.getTeacherId());
                    return course;
                });
        String viewName = controller.saveChangedCourse(form, bindingResultMock, modelMock, authenticationMock);
        assertEquals("redirect:/course/?edited=" + course.getId(), viewName);

        verify(modelMock, times(0)).addAttribute(anyString(), any());
        verify(courseService).saveCourse(any(CourseForm.class));
    }

    @Test
    public void saveChangedCourseHasErrors() {
        BindingResult bindingResultMock = mock(BindingResult.class);
        when(bindingResultMock.hasErrors()).thenReturn(true);

        CoursesController.CreateForm form = new CoursesController
                .CreateForm(0L, "Title", "Description", 0L);
        form.setId(course.getId());
        form.setTitle(course.getTitle());
        form.setDescription(course.getDescription());
        form.setTeacherId(course.getTeacherId());
        when(grantedAuthority.getAuthority()).thenReturn(User.UserRole.Administrator.name());
        when(authenticationMock.getAuthorities()).thenReturn((Collection) Arrays.asList(grantedAuthority));
        when(lessonService.getLessons()).thenReturn(Arrays.asList(lesson));
        when(courseService.saveCourse(any(CourseForm.class))).thenAnswer(
                invocationOnMock -> {
                    CourseForm courseForm = invocationOnMock.getArgument(0, CourseForm.class);
                    assertEquals(form.getId(), courseForm.getId());
                    assertEquals(form.getTitle(), courseForm.getTitle());
                    assertEquals(form.getDescription(), courseForm.getDescription());
                    assertEquals(form.getTeacherId(), courseForm.getTeacherId());
                    return course;
                });
        String viewName = controller.saveChangedCourse(form, bindingResultMock, modelMock, authenticationMock);
        assertEquals("/course/create", viewName);

        verify(modelMock, times(1)).addAttribute(eq("errors"), any());
        verify(courseService, times(0)).saveCourse(any());
    }

    @Test
    public void saveChangedCourseTeacherIdNull() {
        BindingResult bindingResultMock = mock(BindingResult.class);
        when(bindingResultMock.hasErrors()).thenReturn(false);

        CoursesController.CreateForm form = CoursesController.CreateForm
                .builder()
                .id(null)
                .title(course.getTitle())
                .description(course.getDescription())
                .teacherId(course.getTeacherId())
                .build();
        when(grantedAuthority.getAuthority()).thenReturn(User.UserRole.Administrator.name());
        when(authenticationMock.getAuthorities()).thenReturn((Collection) Arrays.asList(grantedAuthority));
        when(lessonService.getLessons()).thenReturn(Arrays.asList(lesson));
        when(courseService.saveCourse(any(CourseForm.class))).thenAnswer(
                invocationOnMock -> {
                    CourseForm courseForm = invocationOnMock.getArgument(0, CourseForm.class);
                    assertEquals(form.getId(), courseForm.getId());
                    assertEquals(form.getTitle(), courseForm.getTitle());
                    assertEquals(form.getDescription(), courseForm.getDescription());
                    assertEquals(form.getTeacherId(), courseForm.getTeacherId());
                    return course;
                });
        String viewName = controller.saveChangedCourse(form, bindingResultMock, modelMock, authenticationMock);
        assertEquals("redirect:/course/", viewName);

        verify(modelMock, times(0)).addAttribute(eq("errors"), any());
        verify(courseService, times(0)).saveCourse(any());
    }
}