package ru.edu.project.frontend;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.core.Authentication;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import ru.edu.project.authorization.UserDetails;
import ru.edu.project.backend.api.courses.Course;
import ru.edu.project.backend.api.courses.CourseService;
import ru.edu.project.backend.api.grades.Grade;
import ru.edu.project.backend.api.grades.GradeForm;
import ru.edu.project.backend.api.grades.GradeService;
import ru.edu.project.backend.api.groups.Group;
import ru.edu.project.backend.api.groups.GroupService;
import ru.edu.project.backend.api.groups.GroupStudent;
import ru.edu.project.backend.api.groups.GroupStudentService;
import ru.edu.project.backend.api.lessons.Lesson;
import ru.edu.project.backend.api.lessons.LessonService;
import ru.edu.project.backend.api.students.User;
import ru.edu.project.backend.api.students.UserService;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;

import static org.mockito.Mockito.*;

public class GradesControllerTest {
    @Mock
    private Model modelMock;

    @Mock
    private Authentication authenticationMock;

    @Mock
    private CourseService courseService;

    @Mock
    private UserService userService;

    @Mock
    private GroupService groupService;

    @Mock
    private GroupStudentService groupStudentService;

    @Mock
    private LessonService lessonService;

    @Mock
    private GradeService gradeService;

    @InjectMocks
    private GradeController controller;

    private Course course = Course
            .builder()
            .id(1L)
            .title("New course")
            .description("description")
            .teacherId(10L)
            .build();

    private User student = User
            .builder()
            .id(10L)
            .name("Ivan")
            .middleName("Ivanovich")
            .surname("Ivanov")
            .role(User.UserRole.Student)
            .build();

    private Group group = Group
            .builder()
            .id(30L)
            .title("AT-11")
            .courseId(1L)
            .started(false)
            .build();

    private Lesson lesson = Lesson
            .builder()
            .id(10L)
            .courseId(1L)
            .title("Title")
            .description("Some description")
            .homework("yes")
            .build();

    private Grade grade = Grade
            .builder()
            .id(1L)
            .lessonId("1")
            .groupStudentId("2")
            .grade("5")
            .build();

    private GroupStudent groupStudent = GroupStudent
            .builder()
            .id(1L)
            .groupId(1L)
            .userId(1L)
            .build();

    @Before
    public void beforeMethod() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void index() {
        List<Course> expectedCoursesList = Arrays.asList(course);
        when(courseService.getCourses()).thenReturn(expectedCoursesList);

        List<GroupStudent> expectedGroupStudentList = Arrays.asList(groupStudent);
        when(groupStudentService.getGroupStudents()).thenReturn(expectedGroupStudentList);

        List<Lesson> expectedLessonsList = Arrays.asList(lesson);
        when(lessonService.getLessons()).thenReturn(expectedLessonsList);

        List<User> expectedUsersList = Arrays.asList(student);
        when(userService.getUsers()).thenReturn(expectedUsersList);

        List<Group> expectedGroupsList = Arrays.asList(group);
        when(groupService.getByCourse(course.getId())).thenReturn(expectedGroupsList);

        List<Grade> expectedGradesList = Arrays.asList(grade);
        when(gradeService.getGrades()).thenReturn(expectedGradesList);

        String viewName = controller.index(modelMock);
        assertEquals("/grade/index", viewName);

        verify(modelMock)
                .addAttribute("grades", expectedGradesList);
    }

    @Test
    public void createForm() {
        String viewName = controller.createForm(modelMock);
        assertEquals("/grade/create", viewName);

        verify(modelMock)
                .addAttribute("activeGrades", true);
    }

    @Test
    public void createFormProcessing() {
        BindingResult bindingResultMock = mock(BindingResult.class);
        when(bindingResultMock.hasErrors()).thenReturn(false);
        GradeController.CreateForm form = new GradeController.CreateForm();
        form.setLessonId("1");
        form.setGroupStudentId("1");
        form.setGrade("5");
        when(gradeService.createGrade(any(GradeForm.class))).thenAnswer(
                invocationOnMock -> {
                    GradeForm gradeForm = invocationOnMock.getArgument(0, GradeForm.class);
                    assertEquals(form.getLessonId(), gradeForm.getLessonId());
                    assertEquals(form.getGroupStudentId(), gradeForm.getGroupStudentId());
                    assertEquals(form.getGrade(), gradeForm.getGrade());
                    return grade;
                });
        String viewName = controller.createFormProcessing(form, bindingResultMock, modelMock);
        assertEquals("redirect:/grade/?created=" + grade.getId(), viewName);

        verify(modelMock)
                .addAttribute("activeGrades", true);
        verify(gradeService).createGrade(any(GradeForm.class));
    }

    @Test
    public void editForm() {
        List<Lesson> expectedLessonsList = Arrays.asList(lesson);
        when(lessonService.getLessons()).thenReturn(expectedLessonsList);

        when(gradeService.getGrade(grade.getId())).thenReturn(grade);

        String viewName = controller.editForm(modelMock, grade.getId());
        assertEquals("/grade/edit", viewName);

        verify(modelMock)
                .addAttribute("grade", grade);
        verify(modelMock)
                .addAttribute("lessons", expectedLessonsList);
        verify(modelMock)
                .addAttribute("activeGrades", true);
    }

    @Test
    public void saveChangedCourse() {
        BindingResult bindingResultMock = mock(BindingResult.class);
        when(bindingResultMock.hasErrors()).thenReturn(false);

        GradeController.CreateForm form = new GradeController.CreateForm();
        form.setId(grade.getId());
        form.setLessonId(grade.getLessonId());
        form.setGroupStudentId(grade.getGroupStudentId());
        form.setGrade(grade.getGrade());

        when(gradeService.saveGrade(any(GradeForm.class))).thenAnswer(
                invocationOnMock -> {
                    GradeForm gradeForm = invocationOnMock.getArgument(0, GradeForm.class);
                    assertEquals(form.getId(), gradeForm.getId());
                    assertEquals(form.getLessonId(), gradeForm.getLessonId());
                    assertEquals(form.getGroupStudentId(), gradeForm.getGroupStudentId());
                    assertEquals(form.getGrade(), gradeForm.getGrade());
                    return grade;
                });
        String viewName = controller.saveChangedGrade(form, bindingResultMock, modelMock);
        assertEquals("redirect:/grade/?edited=" + grade.getId(), viewName);

        verify(gradeService).saveGrade(any(GradeForm.class));
        verify(modelMock)
                .addAttribute("activeGrades", true);
    }

    @Test
    public void students() {
        List<GroupStudent> expectedGroupStudentList = Arrays.asList(groupStudent);
        when(groupStudentService.getGroupStudents()).thenReturn(expectedGroupStudentList);

        String viewName = controller.students(modelMock, "2", "1");
        assertEquals("/grade/students", viewName);

        verify(modelMock)
                .addAttribute("activeGrades", true);

        verify(modelMock)
                .addAttribute("students", expectedGroupStudentList);
    }

    @Test
    public void courses() {
        List<Course> expectedCoursesList = Arrays.asList(course);
        when(courseService.getCourses()).thenReturn(expectedCoursesList);

        String viewName = controller.coursesList(modelMock);
        assertEquals("/grade/courses", viewName);

        verify(modelMock)
                .addAttribute("courses", expectedCoursesList);
        verify(modelMock)
                .addAttribute("activeGrades", true);
    }

    @Test
    public void coursesRedirect() {
        String viewName = controller.coursesList("1", modelMock);
        assertEquals("redirect:/grade/lesson_groups?course=1", viewName);
        verify(modelMock)
                .addAttribute("activeGrades", true);
    }

    @Test
    public void lessonGroupsListGet() {
        List<Group> expectedGroupsList = Arrays.asList(group);
        when(groupService.getGroups()).thenReturn(expectedGroupsList);

        List<Lesson> expectedLessonsList = Arrays.asList(lesson);
        when(lessonService.getLessons()).thenReturn(expectedLessonsList);

        String viewName = controller.lessonGroupsList("1", modelMock);
        assertEquals("/grade/lesson_groups", viewName);

        verify(modelMock)
                .addAttribute("groups", expectedGroupsList);
        verify(modelMock)
                .addAttribute("lessons", expectedLessonsList);
        verify(modelMock)
                .addAttribute("activeGrades", true);
    }

    @Test
    public void lessonGroupsListPost() {
        List<Group> expectedGroupsList = Arrays.asList(group);
        when(groupService.getGroups()).thenReturn(expectedGroupsList);

        List<Lesson> expectedLessonsList = Arrays.asList(lesson);
        when(lessonService.getLessons()).thenReturn(expectedLessonsList);

        String viewName = controller.lessonGroupsList("1", "1", modelMock);
        assertEquals("redirect:/grade/students?lesson=1&group=1", viewName);

        verify(modelMock)
                .addAttribute("activeGrades", true);
    }

    @Test
    public void all() {
        List<Grade> expectedGradesList = Arrays.asList(grade);
        when(gradeService.getGrades()).thenReturn(expectedGradesList);

        String viewName = controller.all(modelMock);
        assertEquals("/grade/index", viewName);

        verify(modelMock)
                .addAttribute("grades", expectedGradesList);
        verify(modelMock)
                .addAttribute("activeGrades", true);
    }

    @Test
    public void delete() {
        List<Grade> expectedGradesList = Arrays.asList(grade);
        when(gradeService.deleteGrade(1L)).thenReturn(expectedGradesList.remove(1L));

        String viewName = controller.delete(1L);
        assertEquals("redirect:/grade/?deleted=false", viewName);
    }

    @Test
    public void student() {
        List<Grade> expectedGradesList = Arrays.asList(grade);
        when(gradeService.getGrades()).thenReturn(expectedGradesList);
    }
}

