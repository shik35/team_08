package ru.edu.project.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;
import ru.edu.project.authorization.FrontendUserService;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    /**
     * encoder.
     */
    @Autowired
    private PasswordEncoder encoder;
    /**
     * Зависимость на реализацию UserDetailService.
     */
    @Autowired
    private FrontendUserService frontendUserService;

    /**
     * Настраиваем параметры доступов к URL.
     *
     * @param http
     * @throws Exception
     */
    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        http
                .anonymous()
                .authorities("ANON")
                .and()
                .authorizeHttpRequests()
                .antMatchers("/").permitAll()
                //.antMatchers("/*").hasAuthority("ANON")
                .antMatchers("/user/create").hasAuthority("Administrator")
                .antMatchers("/user/**").hasAnyAuthority("Administrator", "Student", "Teacher")
                .antMatchers("/grade/create/**").hasAnyAuthority("Administrator", "Teacher")
                .antMatchers("/grade/**").hasAnyAuthority("Student", "Teacher", "Administrator")
                .antMatchers("/course/create/**").hasAnyAuthority("Teacher", "Administrator")
                //.antMatchers("/course/**").hasAnyAuthority("Student", "Teacher", "Administrator")
                .antMatchers("/course/**").permitAll()
                .antMatchers("/course/edit").hasAnyAuthority("Teacher", "Administrator")
                .antMatchers("/lesson/add").hasAnyAuthority("Teacher", "Administrator")
                .antMatchers("/group/**").hasAnyAuthority("Student", "Teacher", "Administrator")
                .antMatchers("/group/create").hasAnyAuthority("Teacher", "Administrator")
                .antMatchers("/group/edit").hasAnyAuthority("Teacher", "Administrator")
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .and()
                .csrf().disable(); //отключаем защиту от Cross-Site Request Forgery
    }

    /**
     * Настраиваем пользователей.
     *
     * @param auth
     * @throws Exception
     */
    @Override
    protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
        /*
        для быстрой отладки можно использовать inMemory реестр пользователей:*/

        /*auth
                .inMemoryAuthentication()
                .withUser("student")
                .password(encoder.encode("student"))
                .authorities("Student")
                .and()
                .withUser("admin")
                .password(encoder.encode("admin"))
                .authorities("Administrator")
                .and()
                .withUser("teacher")
                .password(encoder.encode("teacher"))
                .authorities("Teacher");*/

        auth.userDetailsService(frontendUserService);
    }
}
