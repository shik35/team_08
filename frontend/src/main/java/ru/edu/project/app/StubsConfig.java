package ru.edu.project.app;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import ru.edu.project.backend.api.courses.CourseService;
import ru.edu.project.backend.api.grades.GradeService;
import ru.edu.project.backend.api.groups.GroupService;
import ru.edu.project.backend.api.groups.GroupStudentService;
import ru.edu.project.backend.api.lessons.LessonService;
import ru.edu.project.backend.api.students.UserService;
import ru.edu.project.stub.courses.CourseInMemoryService;
import ru.edu.project.stub.grades.GradeInMemoryService;
import ru.edu.project.stub.groups.GroupInMemoryService;
import ru.edu.project.stub.groups.GroupStudentInMemoryService;
import ru.edu.project.stub.lessons.LessonInMemoryService;
import ru.edu.project.stub.students.UserInMemoryService;

@Configuration
@Profile("STUBS")
public class StubsConfig {

    /**
     * Заглушка сервиса.
     *
     * @return bean
     */
    @Bean
    public UserService userServiceBean() {
        return new UserInMemoryService();
    }

    /**
     * Заглушка для сервиса работы с курсами.
     *
     * @return bean
     */
    @Bean
    public CourseService courseServiceBean() {
        return new CourseInMemoryService();
    }

    /**
     * Заглушка сервиса оценок.
     *
     * @return bean
     */
    @Bean
    public GradeService gradeServiceBean() {
        return new GradeInMemoryService();
    }

    /**
     * Заглушка сервиса уроков.
     *
     * @return bean
     */
    @Bean
    public LessonService lessonServiceBean() {
        return new LessonInMemoryService();
    }

    /**
     * Заглушка сервиса групп.
     *
     * @return bean
     */
    @Bean
    public GroupService groupServiceBean() {
        return new GroupInMemoryService();
    }

    /**
     * Заглушка сервиса группа/студент.
     *
     * @return bean
     */
    @Bean
    public GroupStudentService groupStudentServiceBean() {
        return new GroupStudentInMemoryService();
    }
}
