package ru.edu.project.app;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import ru.edu.project.backend.RestServiceInvocationHandler;
import ru.edu.project.backend.api.courses.CourseService;
import ru.edu.project.backend.api.grades.GradeService;
import ru.edu.project.backend.api.groups.GroupService;
import ru.edu.project.backend.api.groups.GroupStudentService;
import ru.edu.project.backend.api.lessons.LessonService;
import ru.edu.project.backend.api.students.UserService;

import java.lang.reflect.Proxy;

@Configuration
@Profile("REST")
@SuppressWarnings("unchecked")
public class RemoteServiceConfig {

    /**
     * Создаем rest-прокси для UserService.
     *
     * @param handler
     * @return rest-proxy
     */
    @Bean
    public UserService userServiceRest(final RestServiceInvocationHandler handler) {
        handler.setServiceUrl("/user");
        return getProxy(handler, UserService.class);
    }

    private <T> T getProxy(final RestServiceInvocationHandler handler, final Class<T>... tClass) {
        return (T) Proxy.newProxyInstance(this.getClass().getClassLoader(), tClass, handler);
    }

    /**
     * Создает rest-прокси для CourseService.
     *
     * @param handler
     * @return rest-proxy
     */
    @Bean
    public CourseService courseServiceRest(final RestServiceInvocationHandler handler) {
        handler.setServiceUrl("/course");
        return getProxy(handler, CourseService.class);
    }

    /**
     * Создает rest-прокси для GradeService.
     *
     * @param handler
     * @return rest-proxy
     */
    @Bean
    public GradeService gradeServiceRest(final RestServiceInvocationHandler handler) {
        handler.setServiceUrl("/grade");
        return getProxy(handler, GradeService.class);
    }

    /**
     * Создаем rest-прокси для UserService.
     *
     * @param handler
     * @return rest-proxy
     */
    @Bean
    public GroupService groupServiceRest(final RestServiceInvocationHandler handler) {
        handler.setServiceUrl("/group");
        return getProxy(handler, GroupService.class);
    }

    /**
     * Создаем rest-прокси для GroupStudentService.
     *
     * @param handler
     * @return rest-proxy
     */
    @Bean
    public GroupStudentService groupStudentServiceRest(final RestServiceInvocationHandler handler) {
        handler.setServiceUrl("/groupStudent");
        return getProxy(handler, GroupStudentService.class);
    }

    /**
     * Создает rest-прокси для LessonService.
     *
     * @param handler
     * @return rest-proxy
     */
    @Bean
    public LessonService lessonServiceRest(final RestServiceInvocationHandler handler) {
        handler.setServiceUrl("/lesson");
        return getProxy(handler, LessonService.class);
    }
}
