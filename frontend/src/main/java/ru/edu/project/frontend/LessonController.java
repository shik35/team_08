package ru.edu.project.frontend;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.edu.project.authorization.UserDetails;
import ru.edu.project.backend.api.courses.CourseService;
import ru.edu.project.backend.api.lessons.LessonForm;
import ru.edu.project.backend.api.lessons.LessonService;
import ru.edu.project.backend.api.students.User;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RequestMapping("/lesson")
@Controller
public class LessonController {

    /**
     * Логгер.
     */
    private static final Logger LOG = LoggerFactory.getLogger(LessonController.class);

    /**
     * Служба получения курсов.
     */
    @Autowired
    private CourseService courseService;


    /**
     * Служба получения уроков.
     */
    @Autowired
    private LessonService lessonService;

    /**
     * Получить страницу с формой добавления урока.
     *
     * @param model
     * @param courseId
     * @param authentication
     * @return String
     */
    @GetMapping("/add")
    public String addLessonForm(final Model model,
                                @RequestParam final Long courseId,
                                final Authentication authentication) {
        List<String> rolesList = getRolesList(authentication);
        if (rolesList.contains(User.UserRole.Administrator.name())) {
            return "/lesson/edit_lesson";
        } else if (rolesList.contains(User.UserRole.Teacher.name())) {
            if (courseService.getCourse(courseId).getTeacherId()
                    == ((UserDetails) authentication.getPrincipal()).getId()) {
                return "/lesson/edit_lesson";
            }
        }
        model.addAttribute("error", "Доступ запрещен");
        return "/common/error_page";
    }

    /**
     * Получить страницу с формой добавления урока.
     *
     * @param form
     * @param model
     * @param courseId
     * @param authentication
     * @return String
     */
    @PostMapping("/add")
    public String saveLesson(@Valid @ModelAttribute final CreateForm form, final Model model,
                             @RequestParam final Long courseId, final Authentication authentication) {
        LessonForm lessonForm = LessonForm
                .builder()
                .courseId(courseId)
                .title(form.getTitle())
                .description(form.getDescription())
                .homework(form.getHomework())
                .build();
        lessonService.createLesson(lessonForm);
        return "redirect:/course/edit?id=" + courseId;
    }

    /**
     * Вернем список ролей для объекта authentication.
     *
     * @param authentication
     * @return List<String>
     */
    private List<String> getRolesList(final Authentication authentication) {
        return authentication
                .getAuthorities()
                .stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());
    }


    /**
     * CreateForm class.
     */
    @Getter
    @Setter
    @ToString
    @Builder
    public static class CreateForm {

        /**
         * id урока.
         */
        private Long id;


        /**
         * id курса.
         */
        private Long courseId;

        /**
         * Наименование урока.
         */
        private String title;

        /**
         * Описание урока.
         */
        private String description;

        /**
         * Домашнее задание.
         */
        private String homework;
    }
}

