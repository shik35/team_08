package ru.edu.project.frontend;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.edu.project.backend.api.students.User;
import ru.edu.project.backend.api.students.UserForm;
import ru.edu.project.backend.api.students.UserService;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@RequestMapping("/user")
@Controller
public class UserController {
    /**
     * service.
     */
    @Autowired
    private UserService service;

    /**
     * password encoder.
     */
    @Autowired
    private PasswordEncoder encoder;

    /**
     * index GET method.
     * @param model
     * @return String
     */
    @GetMapping("/")
    public String index(final Model model) {
        model.addAttribute("users", service.getUsers());
        return "/user/index";
    }

    /**
     * create GET method.
     * @param model
     * @return String.
     */
    @GetMapping("/create")
    public String createForm(final Model model) {
        model.addAttribute("roles", User.UserRole.values());
        return "/user/create";
    }

    /**
     * create POST method.
     * @param form
     * @param bindingResult
     * @param model
     * @return String
     */
    @PostMapping("/create")
    public String createFormProcessing(
            @Valid
            @ModelAttribute final CreateForm form,
            final BindingResult bindingResult,
            final Model model
    ) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("errors", bindingResult.getAllErrors());
            return createForm(model);
        }
        User user = service.createUser(UserForm.builder().
                name(form.getName()).
                surname(form.getSurname()).
                middleName(form.getMiddleName()).
                role(form.getRole()).
                login(form.getLogin()).
                password(encoder.encode(form.getPassword())).
                build());
        return "redirect:/user/?created=" + user.getId();
    }

    /**
     * CreateForm class.
     */
    @Getter
    @Setter
    public static class CreateForm {
        /**
         * Имя.
         */
        @NotEmpty
        @NotNull
        private String name;

        /**
         * Фамилия.
         */
        @NotNull
        @NotEmpty
        private String surname;

        /**
         * Отчество.
         */
        private String middleName;

        /**
         * Роль.
         */
        @NotNull
        private User.UserRole role;

        /**
         * Логин.
         */
        @NotNull
        @NotEmpty
        private String login;
        /**
         * Пароль.
         */
        @NotEmpty
        @NotNull
        private String password;
    }
}
