package ru.edu.project.frontend;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.edu.project.backend.api.courses.Course;
import ru.edu.project.backend.api.courses.CourseService;
import ru.edu.project.backend.api.groups.Group;
import ru.edu.project.backend.api.groups.GroupForm;
import ru.edu.project.backend.api.groups.GroupService;
import ru.edu.project.backend.api.groups.GroupStudent;
import ru.edu.project.backend.api.groups.GroupStudentService;
import ru.edu.project.backend.api.groups.GroupStudentForm;
import ru.edu.project.backend.api.students.User;
import ru.edu.project.backend.api.students.UserService;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@RequestMapping("/group")
@Controller
public class GroupController {

    /**
     * Логгер.
     */
    private static final Logger LOG = LoggerFactory.getLogger(GroupController.class);

    /**
     * Служба получения курсов.
     */
    @Autowired
    private CourseService courseService;

    /**
     * Служба получения пользователей.
     */
    @Autowired
    private UserService userService;

    /**
     * Служба получения групп.
     */
    @Autowired
    private GroupService groupService;


    /**
     * Служба получения студентов групп.
     */
    @Autowired
    private GroupStudentService groupStudentService;

    /**
     * Получить страницу со списком курсов.
     *
     * @param model
     * @return String
     */
    @GetMapping("/")
    public String index(final Model model) {

        List<Course> courses =
                courseService.getCourses();

        List<Group> groups =
                groupService.getGroups();

        Map<Long, Course> coursesMap = courses
                .stream()
                .collect(Collectors.toMap(Course::getId, Function.identity()));
        Map<Long, Integer> groupStudents = new HashMap<>();

        groups.forEach(group -> {
            groupStudents.put(group.getId(),
                    groupStudentService.getByGroup(group.getId()).size());
        });

        model.addAttribute("groups", groups);
        model.addAttribute("courses", coursesMap);
        model.addAttribute("groupStudents", groupStudents);

        return "/group/index";
    }


    /**
     * Страница просмотра группы.
     *
     * @param model
     * @param id
     * @return String
     */
    @GetMapping("/{id}")
    public String view(final Model model, @PathVariable final Long id) {

        Group group = groupService.getGroup(id);
        Course course = courseService.getCourse(group.getCourseId());

        List<User> users =
                userService.getUsers();
        Map<Long, User> usersMap = users
                .stream()
                .collect(Collectors.toMap(User::getId, Function.identity()));

        User teacher = usersMap.get(course.getTeacherId());

        StringBuilder teacherName = new StringBuilder();
        if (teacher != null) {
            if (teacher.getSurname() != null) {
                teacherName.append(teacher.getSurname());
            }
            if (teacher.getName() != null) {
                teacherName.append(' ' + teacher.getName());
            }
            if (teacher.getMiddleName() != null) {
                teacherName.append(' ' + teacher.getMiddleName());
            }
        }

        List<GroupStudent> groupStudents = groupStudentService.getByGroup(id);

        Map<Long, GroupStudent> groupStudentsMap = groupStudents
                .stream()
                .collect(Collectors.toMap(GroupStudent::getUserId, Function.identity()));

        List<User> groupStudentsToAdd = new ArrayList<>();
        users.forEach(user -> {
            if (groupStudentsMap.get(user.getId()) == null
                    && user.getRole().equals(User.UserRole.Student)) {
                groupStudentsToAdd.add(user);
            }
        });

        model.addAttribute("group", group);
        model.addAttribute("course", course);
        model.addAttribute("teacher", teacherName);
        model.addAttribute("users", usersMap);
        model.addAttribute("groupStudents", groupStudents);
        model.addAttribute("addStudents", groupStudentsToAdd);

        return "/group/view";
    }

    /**
     * Сохранение созданной группы.
     *
     * @param form
     * @param bindingResult
     * @param model
     * @param authentication
     * @return String
     */
    @PostMapping("/{id}")
    public String addGroupStudent(@Valid @ModelAttribute final CreateGroupStudentForm form,
                                   final BindingResult bindingResult,
                                   final Model model,
                                   final Authentication authentication) {

        LOG.info("Method started: addGroupStudent");

        if (bindingResult.hasErrors()) {
            model.addAttribute("errors", bindingResult.getAllErrors());
            return "redirect:/group/" + form.getGroupId();
        }

        LOG.info("Student should be added to group: {}", form);
        GroupStudent createdGroupStudent = groupStudentService.addStudentToGroup(
                GroupStudentForm
                        .builder()
                        .groupId(form.getGroupId())
                        .userId(form.getUserId())
                        .build());

        LOG.info("Created group: {}", createdGroupStudent);
        return "redirect:/group/" + createdGroupStudent.getGroupId();

    }




    /**
     * Получить страницу с формой создания новой группы.
     *
     * @param model
     * @param authentication
     * @return String
     */
    @GetMapping("/create")
    public String createGroup(final Model model, final Authentication authentication) {

        List<Course> courses =
                courseService.getCourses();

        model.addAttribute("courses", courses);
        return "/group/create";

    }


    /**
     * Сохранение созданной группы.
     *
     * @param form
     * @param bindingResult
     * @param model
     * @param authentication
     * @return String
     */
    @PostMapping("/create")
    public String saveCreatedGroup(@Valid @ModelAttribute final CreateGroupForm form,
                            final BindingResult bindingResult,
                            final Model model,
                            final Authentication authentication) {

        LOG.info("Method started: saveCreatedGroup");

        if (bindingResult.hasErrors()) {
            model.addAttribute("errors", bindingResult.getAllErrors());
            return createGroup(model, authentication);
        }

        if (form.getTitle() != null) {
            LOG.info("Group should be created: {}", form);

            Group createdGroup = groupService.createGroup(
                    GroupForm
                            .builder()
                            .title(form.getTitle())
                            .courseId(form.getCourseId())
                            .started(form.isStarted())
                            .build());

            LOG.info("Created group: {}", createdGroup);
            return "redirect:/group/" + createdGroup.getId();
        } else {
            return "redirect:/group/";
        }

    }


    /**
     * Получить страницу с формой редактирования группы.
     *
     * @param model
     * @param id
     * @param authentication
     * @return String.
     */
    @GetMapping("/edit")
    public String editGroup(final Model model,
                            @RequestParam final Long id,
                            final Authentication authentication) {

        Group group = groupService.getGroup(id);

        List<Course> courses =
                courseService.getCourses();

        model.addAttribute("group", group);
        model.addAttribute("courses", courses);
        return "/group/edit";

    }




    /**
     * Сохранение отредактированной группы.
     *
     * @param form
     * @param bindingResult
     * @param model
     * @param authentication
     * @return String
     */
    @PostMapping("/edit")
    public String saveGroup(@Valid @ModelAttribute final CreateGroupForm form,
                                    final BindingResult bindingResult,
                                    final Model model,
                                    final Authentication authentication) {

            LOG.info("Method started: saveGroup");
            if (bindingResult.hasErrors()) {
                model.addAttribute("errors", bindingResult.getAllErrors());
                return editGroup(model, form.id, authentication);
            }

            if (form.getId() != null) {
                LOG.info("Group should be changed: {}", form);

                Group changedGroup = groupService.updateGroup(
                        GroupForm
                                .builder()
                                .id(form.getId())
                                .title(form.getTitle())
                                .courseId(form.getCourseId())
                                .started(form.isStarted())
                                .build());

                LOG.info("Changed group: {}", changedGroup);
                return "redirect:/group/" + changedGroup.getId();
            } else {
                return "redirect:/group/";
            }

    }



    /**
     * CreateGroupForm class.
     */
    @Getter
    @Setter
    @ToString
    public static class CreateGroupForm {

        /**
         * id курса.
         */
        private Long id;

        /**
         * Id курса.
         */
        @NotNull
        private Long courseId;

        /**
         * /**
         * Наименование группы.
         */
        @NotNull
        @NotEmpty
        private String title;

        /**
         * Курс начался?
         */
        private boolean started;
    }

    /**
     * CreateGroupStudentForm class.
     */
    @Getter
    @Setter
    @ToString
    public static class CreateGroupStudentForm {

        /**
         * id курса.
         */
        private Long id;

        /**
         * Id студента.
         */
        @NotNull
        private Long userId;

        /**
         * Id группы.
         */
        @NotNull
        private Long groupId;
    }

}
