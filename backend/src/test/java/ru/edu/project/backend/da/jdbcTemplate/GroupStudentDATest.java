package ru.edu.project.backend.da.jdbcTemplate;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import ru.edu.project.backend.api.groups.GroupStudent;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GroupStudentDATest {

    public static final long ID = 1L;
    public static final long GROUP_ID = 1L;
    public static final long USER_ID = 1L;

    @Mock
    private JdbcTemplate jdbcTemplate;

    @Mock
    private NamedParameterJdbcTemplate jdbcUpdate;

    @InjectMocks
    private GroupStudentDA da;

    private GroupStudent mockGroupStudent =
            GroupStudent
                    .builder()
                    .id(ID)
                    .groupId(GROUP_ID)
                    .userId(USER_ID)
                    .build();

    @Test
    public void insert() {

        SimpleJdbcInsert jdbcInsert = mock(SimpleJdbcInsert.class);
        when(jdbcInsert.withTableName(any(String.class))).thenReturn(jdbcInsert);
        when(jdbcInsert.usingGeneratedKeyColumns(any(String.class))).thenReturn(jdbcInsert);
        when(jdbcInsert.executeAndReturnKey(any(Map.class))).thenReturn(1L);

        da.setJdbcInsert(jdbcInsert);

        GroupStudent mockGroupStudent =
                GroupStudent.builder()
                        .groupId(GROUP_ID)
                        .userId(USER_ID)
                        .build();

        GroupStudent groupStudent = da.save(mockGroupStudent);
        assertEquals(GROUP_ID, groupStudent.getGroupId().longValue());
        assertEquals(USER_ID, groupStudent.getUserId().longValue());

    }

    @Test
    public void getById() throws SQLException {

        ResultSet rs = mock(ResultSet.class);
        when(rs.next()).thenReturn(true);
        when(rs.getLong("group_student_id")).thenReturn(ID);
        when(rs.getLong("group_student_group_id")).thenReturn(GROUP_ID);
        when(rs.getLong("group_student_user_id")).thenReturn(USER_ID);

        Answer<GroupStudent> answer = invocationOnMock -> {
            ResultSetExtractor<GroupStudent> extractor = invocationOnMock.getArgument(1);
            return extractor.extractData(rs);
        };

        when(jdbcTemplate.query(
                any(String.class),
                any(ResultSetExtractor.class),
                any(Long.class))).thenAnswer(answer);

        GroupStudent groupStudent = da.getById(ID);
        assertEquals(mockGroupStudent.getId(), groupStudent.getId());
        assertEquals(mockGroupStudent.getGroupId(), groupStudent.getGroupId());
        assertEquals(mockGroupStudent.getUserId(), groupStudent.getUserId());

    }

    @Test
    public void getGroupStudents() throws SQLException {

        Answer<List<GroupStudent>> answer = getListAnswer();

        when(jdbcTemplate.query(
                any(String.class),
                any(RowMapper.class))).thenAnswer(answer);

        List<GroupStudent> groupStudents = da.getGroupStudents();
        assertEquals(1, groupStudents.size());

        assertEquals(mockGroupStudent.getId(), groupStudents.get(0).getId());
        assertEquals(mockGroupStudent.getUserId(), groupStudents.get(0).getUserId());
        assertEquals(mockGroupStudent.getGroupId(), groupStudents.get(0).getGroupId());

    }

    @Test
    public void getByGroup() throws SQLException {

        Answer<List<GroupStudent>> answer = getListAnswer();

        when(jdbcTemplate.query(
                any(String.class),
                any(RowMapper.class),
                any(Long.class))).thenAnswer(answer);

        List<GroupStudent> groupStudents = da.getByGroup(GROUP_ID);

        assertEquals(1, groupStudents.size());
        assertEquals(mockGroupStudent.getId(), groupStudents.get(0).getId());
        assertEquals(mockGroupStudent.getGroupId(), groupStudents.get(0).getGroupId());
        assertEquals(mockGroupStudent.getUserId(), groupStudents.get(0).getUserId());

    }

    private Answer<List<GroupStudent>> getListAnswer() throws SQLException {

        ResultSet rs = mock(ResultSet.class);
        when(rs.getLong("group_student_id")).thenReturn(ID);
        when(rs.getLong("group_student_group_id")).thenReturn(GROUP_ID);
        when(rs.getLong("group_student_user_id")).thenReturn(USER_ID);

        Answer<List<GroupStudent>> answer = invocationOnMock -> {
            RowMapper<GroupStudent> mapper = invocationOnMock.getArgument(1);
            return Arrays.asList(mapper.mapRow(rs, 1));
        };

        return answer;
    }



}
