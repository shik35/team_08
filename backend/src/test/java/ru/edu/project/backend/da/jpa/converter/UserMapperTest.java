package ru.edu.project.backend.da.jpa.converter;

import org.junit.Test;
import org.mapstruct.factory.Mappers;
import ru.edu.project.backend.api.students.User;
import ru.edu.project.backend.da.jpa.entity.UserEntity;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class UserMapperTest {
    private UserMapper mapper = Mappers.getMapper(UserMapper.class);
    private User user = User.builder().name("name").role(User.UserRole.Student).build();

    @Test
    public void mapToEntity() {
        UserEntity entity = mapper.map(user);
        assertEquals(entity.getName(), user.getName());
        assertEquals(entity.getRole(), user.getRole());
    }

    @Test
    public void testToUser() {
        UserEntity entity = mapper.map(user);
        User newUser = mapper.map(entity);
        assertEquals(newUser.getName(), entity.getName());
        assertEquals(newUser.getRole(), entity.getRole());
    }

    @Test
    public void mapList() {
        UserEntity entity = mapper.map(user);
        List<UserEntity> entities = Arrays.asList(entity);
        List<User> users = mapper.mapList(entities);
        assertEquals(1, users.size());
        assertEquals(entity.getName(), users.get(0).getName());
    }
}