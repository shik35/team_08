package ru.edu.project.backend.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import ru.edu.project.backend.api.lessons.Lesson;
import ru.edu.project.backend.api.lessons.LessonForm;
import ru.edu.project.backend.da.LessonDALayer;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class LessonServiceLayerTest {

    @Mock
    private LessonDALayer mockDALayer;

    @InjectMocks
    private LessonServiceLayer serviceLayer;

    private Lesson lesson = Lesson
            .builder()
            .id(1L)
            .courseId(2L)
            .title("Lesson")
            .description("Description")
            .homework("Рщьуцщкл")
            .build();

    @Test
    public void createLesson() {
        when(mockDALayer.save(any(Lesson.class))).thenReturn(lesson);
        LessonForm form = LessonForm.builder().build();
        Lesson newLesson = serviceLayer.createLesson(form);
        assertEquals(lesson.getTitle(), newLesson.getTitle());
    }

    @Test
    public void getLessons() {
        when(mockDALayer.getLessons()).thenReturn(Arrays.asList(lesson));
        List<Lesson> lessons = serviceLayer.getLessons();
        assertEquals(1, lessons.size());
        assertEquals(lesson.getTitle(), lessons.get(0).getTitle());
    }

    @Test
    public void saveLesson() {
        when(mockDALayer.save(any(Lesson.class))).thenReturn(lesson);
        LessonForm form = LessonForm.builder().build();
        Lesson updatedLesson = serviceLayer.saveLesson(form);
        assertEquals(lesson.getTitle(), updatedLesson.getTitle());
    }

    @Test
    public void getLessonById() {
        when(mockDALayer.getById(any(Long.class))).thenReturn(lesson);
        Lesson lesson1 = serviceLayer.getLesson(1L);
        assertEquals(lesson.getTitle(), lesson1.getTitle());
    }
}