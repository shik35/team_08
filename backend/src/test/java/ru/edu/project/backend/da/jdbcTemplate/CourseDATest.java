package ru.edu.project.backend.da.jdbcTemplate;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import ru.edu.project.backend.api.courses.Course;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CourseDATest {
    @Mock
    private JdbcTemplate jdbcTemplate;

    @Mock
    private NamedParameterJdbcTemplate jdbcUpdate;

    @InjectMocks
    private CourseDA da;

    private Course mockCourse = Course
            .builder()
            .id(10L)
            .title("Course")
            .description("Course description")
            .teacherId(20L)
            .build();

    @Test
    public void getCourses() throws SQLException {
        ResultSet rs = mock(ResultSet.class);
        when(rs.getLong("id")).thenReturn(10L);
        when(rs.getString("title")).thenReturn("Course");
        when(rs.getString("course_description")).thenReturn("Course description");
        when(rs.getLong("course_teacher_user_id")).thenReturn(20L);
        Answer<List<Course>> answer = new Answer<List<Course>>() {
            public List<Course> answer(InvocationOnMock invocationOnMock) throws Throwable {
                RowMapper<Course> mapper = invocationOnMock.getArgument(1);
                Course course = mapper.mapRow(rs, 1);
                return Arrays.asList(course);
            }
        };
        when(jdbcTemplate.query(any(String.class), any(RowMapper.class))).thenAnswer(answer);
        List<Course> courses = da.getCourses();
        assertEquals(1, courses.size());
        assertEquals(mockCourse.getTitle(), courses.get(0).getTitle());
    }

    @Test
    public void getById() throws SQLException {
        ResultSet rs = mock(ResultSet.class);
        when(rs.getLong("id")).thenReturn(10L);
        when(rs.getString("title")).thenReturn("Course");
        when(rs.getString("course_description")).thenReturn("Course description");
        when(rs.getLong("course_teacher_user_id")).thenReturn(20L);
        Answer<Course> answer = new Answer<Course>() {
            public Course answer(InvocationOnMock invocationOnMock) throws Throwable {
                ResultSetExtractor<Course> extractor = invocationOnMock.getArgument(1);
                Course course = extractor.extractData(rs);
                return course;
            }
        };
        when(jdbcTemplate.query(any(String.class), any(ResultSetExtractor.class), any(Long.class))).thenAnswer(answer);
        Course course = da.getById(10L);
        assertEquals(mockCourse.getId(), course.getId());
        assertEquals(mockCourse.getTitle(), course.getTitle());
        assertEquals(mockCourse.getTeacherId(), course.getTeacherId());
        assertEquals(mockCourse.getDescription(), course.getDescription());
    }

    @Test
    public void save() {
        SimpleJdbcInsert jdbcInsert = mock(SimpleJdbcInsert.class);
        when(jdbcInsert.withTableName(any(String.class))).thenReturn(jdbcInsert);
        when(jdbcInsert.usingGeneratedKeyColumns(any(String.class))).thenReturn(jdbcInsert);
        da.setJdbcInsert(jdbcInsert);
        Course course = da.save(mockCourse);
        assertEquals(10, course.getId().longValue());

        when(jdbcUpdate.update(any(String.class), any(Map.class))).thenReturn(1);
        Course newCourse = da.save(course);
        assertEquals(10, newCourse.getId().longValue());
    }

    @Test
    public void insert() {
        SimpleJdbcInsert jdbcInsert = mock(SimpleJdbcInsert.class);
        when(jdbcInsert.withTableName(any(String.class))).thenReturn(jdbcInsert);
        when(jdbcInsert.usingGeneratedKeyColumns(any(String.class))).thenReturn(jdbcInsert);
        when( jdbcInsert.executeAndReturnKey(any(Map.class))).thenReturn(22L);
        da.setJdbcInsert(jdbcInsert);

        Course courseToInsert = Course
                .builder()
                .title("Course")
                .description("Course description")
                .teacherId(20L)
                .build();

        Course course = da.save(courseToInsert);
        assertEquals(22, course.getId().longValue());
    }
}