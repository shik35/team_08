package ru.edu.project.backend.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import ru.edu.project.backend.api.students.User;
import ru.edu.project.backend.api.students.UserForm;
import ru.edu.project.backend.da.UserDALayer;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceLayerTest {
    @Mock
    private UserDALayer mockDALayer;

    @InjectMocks
    private UserServiceLayer serviceLayer;

    private User user = User.builder().name("1").role(User.UserRole.Student).build();

    @Test
    public void createUser() {
        when(mockDALayer.save(any(User.class))).thenReturn(user);
        UserForm form = UserForm.builder().build();
        User newUser = serviceLayer.createUser(form);
        assertEquals(user.getName(), newUser.getName());
    }

    @Test
    public void getUsers() {
        when(mockDALayer.getUsers()).thenReturn(Arrays.asList(user));
        List<User> users = serviceLayer.getUsers();
        assertEquals(1, users.size());
        assertEquals(user.getName(), users.get(0).getName());
    }

    @Test
    public void updateUser() {
        when(mockDALayer.save(any(User.class))).thenReturn(user);
        UserForm form = UserForm.builder().build();
        User updatedUser = serviceLayer.updateUser(1L, form);
        assertEquals(user.getName(), updatedUser.getName());
    }

    @Test
    public void getUserByLogin() {
        when(mockDALayer.getByLogin(any(String.class))).thenReturn(user);
        User user1 = serviceLayer.getUserByLogin("1");
        assertEquals(user.getName(), user1.getName());
    }
}