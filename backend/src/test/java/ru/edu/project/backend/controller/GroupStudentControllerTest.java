package ru.edu.project.backend.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import ru.edu.project.backend.api.groups.GroupStudent;
import ru.edu.project.backend.api.groups.GroupStudentForm;
import ru.edu.project.backend.api.groups.GroupStudentService;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@WebMvcTest(GroupStudentController.class)
public class GroupStudentControllerTest {

    public static final long ID = 1L;
    public static final long GROUP_ID = 1L;
    public static final long USER_ID = 1L;

    @Autowired
    private MockMvc mockMvc;

    @Mock
    private GroupStudentService groupStudentService;

    @InjectMocks
    private GroupStudentController controller;

    private GroupStudent mockGroupStudent =
            GroupStudent
                    .builder()
                    .id(ID)
                    .groupId(GROUP_ID)
                    .userId(USER_ID)
                    .build();

    public static final String GROUP_STUDENT_STRING = "{\"id\":" + ID
            + ",\"groupId\":" + GROUP_ID
            + ",\"userId\":" + USER_ID + "}";
    public static final String GROUP_STUDENT_ARR = "[" + GROUP_STUDENT_STRING + "]";

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Test
    public void addStudentToGroup() throws Exception {

        when(groupStudentService
                .addStudentToGroup(any(GroupStudentForm.class)))
                .thenReturn(mockGroupStudent);

        String json = "{}";
        RequestBuilder builder =
                MockMvcRequestBuilders.post("/groupStudent/addStudentToGroup")
                        .accept(MediaType.APPLICATION_JSON)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(builder).andReturn();

        assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus());
        assertEquals(GROUP_STUDENT_STRING, result.getResponse().getContentAsString());

    }

    @Test
    public void getGroupStudents() throws Exception {

        when(groupStudentService
                .getGroupStudents())
                .thenReturn(Arrays.asList(mockGroupStudent));

        RequestBuilder builder =
                MockMvcRequestBuilders.get("/groupStudent/getGroupStudents")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(builder).andReturn();

        assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus());
        assertEquals(GROUP_STUDENT_ARR, result.getResponse().getContentAsString());

    }

    @Test
    public void getByGroup() throws Exception {

        when(groupStudentService
                .getByGroup(any(Long.class)))
                .thenReturn(Arrays.asList(mockGroupStudent));

        RequestBuilder builder =
                MockMvcRequestBuilders.get("/groupStudent/getByGroup/" + GROUP_ID)
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(builder).andReturn();

        assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus());
        assertEquals(GROUP_STUDENT_ARR, result.getResponse().getContentAsString());

    }

    @Test
    public void getGroupStudent() throws Exception {

        when(groupStudentService
                .getGroupStudent(any(Long.class)))
                .thenReturn(mockGroupStudent);

        RequestBuilder builder =
                MockMvcRequestBuilders.get("/groupStudent/getGroupStudent/" + ID)
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(builder).andReturn();

        assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus());
        assertEquals(GROUP_STUDENT_STRING, result.getResponse().getContentAsString());

    }

    @Test
    public void deleteStudentFromGroup() throws Exception {

        when(groupStudentService
                .deleteStudentFromGroup(any(Long.class), any(Long.class)))
                .thenReturn(false);

        String json = "{}";
        RequestBuilder builder =
                MockMvcRequestBuilders
                        .post("/groupStudent/deleteStudentFromGroup?groupId=" + GROUP_ID + "&userId=" + USER_ID)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(builder).andReturn();

        assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus());
        assertEquals("false", result.getResponse().getContentAsString());

    }

}