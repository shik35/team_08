package ru.edu.project.backend.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import ru.edu.project.backend.api.grades.Grade;
import ru.edu.project.backend.api.grades.GradeForm;
import ru.edu.project.backend.da.GradeDALayer;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GradeServiceLayerTest {

    @Mock
    private GradeDALayer mockDALayer;

    @InjectMocks
    private GradeServiceLayer serviceLayer;

    private Grade grade = Grade
            .builder()
            .id(1L)
            .lessonId("2")
            .groupStudentId("3")
            .grade("5")
            .build();

    @Test
    public void createGrade() {
        when(mockDALayer.save(any(Grade.class))).thenReturn(grade);
        GradeForm form = GradeForm.builder().build();
        Grade newGrade = serviceLayer.createGrade(form);
        assertEquals(grade.getGrade(), newGrade.getGrade());
    }

    @Test
    public void getGrades() {
        when(mockDALayer.getGrades()).thenReturn(Arrays.asList(grade));
        List<Grade> grades = serviceLayer.getGrades();
        assertEquals(1, grades.size());
        assertEquals(grade.getGrade(), grades.get(0).getGrade());
    }

    @Test
    public void saveGrade() {
        when(mockDALayer.save(any(Grade.class))).thenReturn(grade);
        GradeForm form = GradeForm.builder().build();
        Grade updatedGrade = serviceLayer.saveGrade(form);
        assertEquals(grade.getGrade(), updatedGrade.getGrade());
    }

    @Test
    public void getGradeById() {
        when(mockDALayer.getById(any(Long.class))).thenReturn(grade);
        Grade grade1 = serviceLayer.getGrade(1L);
        assertEquals(grade.getGrade(), grade1.getGrade());
    }
}
