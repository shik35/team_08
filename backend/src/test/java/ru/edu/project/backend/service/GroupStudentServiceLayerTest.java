package ru.edu.project.backend.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import ru.edu.project.backend.api.groups.Group;
import ru.edu.project.backend.api.groups.GroupForm;
import ru.edu.project.backend.api.groups.GroupStudent;
import ru.edu.project.backend.api.groups.GroupStudentForm;
import ru.edu.project.backend.da.GroupDALayer;
import ru.edu.project.backend.da.GroupStudentDALayer;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GroupStudentServiceLayerTest {

    public static final long ID = 1L;
    public static final long GROUP_ID = 1L;
    public static final long USER_ID = 1L;

    @Mock
    private GroupStudentDALayer mockDALayer;

    @InjectMocks
    private GroupStudentServiceLayer serviceLayer;

    private GroupStudent groupStudent =
            GroupStudent
                    .builder()
                    .id(ID)
                    .groupId(GROUP_ID)
                    .userId(USER_ID)
                    .build();

    @Test
    public void addStudentToGroup() {

        when(mockDALayer.save(any(GroupStudent.class))).thenReturn(groupStudent);

        GroupStudentForm form = GroupStudentForm.builder().build();
        GroupStudent newGroupStudent = serviceLayer.addStudentToGroup(form);

        assertEquals(groupStudent.getUserId(), newGroupStudent.getUserId());
        assertEquals(groupStudent.getGroupId(), newGroupStudent.getGroupId());

    }

    @Test
    public void getGroupStudents() {

        when(mockDALayer.getGroupStudents()).thenReturn(Arrays.asList(groupStudent));

        List<GroupStudent> groupStudents = serviceLayer.getGroupStudents();

        assertEquals(1, groupStudents.size());
        assertEquals(groupStudent.getUserId(), groupStudents.get(0).getUserId());
        assertEquals(groupStudent.getGroupId(), groupStudents.get(0).getGroupId());
    }

    @Test
    public void getByGroup() {

        when(mockDALayer.getByGroup(any(Long.class))).thenReturn(Arrays.asList(groupStudent));

        List<GroupStudent> groupStudents = serviceLayer.getByGroup(GROUP_ID);

        assertEquals(1, groupStudents.size());
        assertEquals(groupStudent.getUserId(), groupStudents.get(0).getUserId());
        assertEquals(groupStudent.getGroupId(), groupStudents.get(0).getGroupId());
    }

    @Test
    public void getById() {

        when(mockDALayer.getById(any(Long.class))).thenReturn(groupStudent);

        GroupStudent newGroupStudent = serviceLayer.getGroupStudent(ID);

        assertEquals(groupStudent.getUserId(), newGroupStudent.getUserId());
        assertEquals(groupStudent.getGroupId(), newGroupStudent.getGroupId());

    }
}
