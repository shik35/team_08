package ru.edu.project.backend.da.jdbcTemplate;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import ru.edu.project.backend.api.students.User;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class UserDATest {
    @Mock
    private JdbcTemplate jdbcTemplate;

    @Mock
    private NamedParameterJdbcTemplate jdbcUpdate;

    @InjectMocks
    private UserDA da;

    private User mockUser = User.builder().name("1").role(User.UserRole.Student).build();

    @Test
    public void getUsers() throws SQLException {
        ResultSet rs = mock(ResultSet.class);
        when(rs.getLong(any(String.class))).thenReturn(1L);
        when(rs.getString("name")).thenReturn(null);
        when(rs.getString("surname")).thenReturn(null);
        when(rs.getString("middle_name")).thenReturn(null);
        when(rs.getString("role")).thenReturn("Student");
        when(rs.getString("login")).thenReturn(null);
        when(rs.getString("password")).thenReturn(null);
        Answer<List<User>> answer = new Answer<List<User>>() {
            public List<User> answer(InvocationOnMock invocationOnMock) throws Throwable {
                RowMapper<User> mapper = invocationOnMock.getArgument(1);
                User user = mapper.mapRow(rs, 1);
                return Arrays.asList(user);
            }
        };
        when(jdbcTemplate.query(any(String.class), any(RowMapper.class))).thenAnswer(answer);
        List<User> users = da.getUsers();
        assertEquals(1, users.size());
        assertEquals(mockUser.getRole(), users.get(0).getRole());
    }

    @Test
    public void getById() throws SQLException {
        ResultSet rs = mock(ResultSet.class);
        when(rs.next()).thenReturn(true);
        when(rs.getLong(any(String.class))).thenReturn(1L);
        when(rs.getString("name")).thenReturn(null);
        when(rs.getString("surname")).thenReturn(null);
        when(rs.getString("middle_name")).thenReturn(null);
        when(rs.getString("role")).thenReturn("Student");
        when(rs.getString("login")).thenReturn(null);
        when(rs.getString("password")).thenReturn(null);
        Answer<User> answer = new Answer<User>() {
            public User answer(InvocationOnMock invocationOnMock) throws Throwable {
                ResultSetExtractor<User> extractor = invocationOnMock.getArgument(1);
                User user = extractor.extractData(rs);
                return user;
            }
        };
        when(jdbcTemplate.query(any(String.class), any(ResultSetExtractor.class), any(Long.class))).thenAnswer(answer);
        User user = da.getById(1L);
        assertEquals(mockUser.getRole(), user.getRole());
    }

    @Test
    public void save() {
        SimpleJdbcInsert jdbcInsert = mock(SimpleJdbcInsert.class);
        when(jdbcInsert.withTableName(any(String.class))).thenReturn(jdbcInsert);
        when(jdbcInsert.usingGeneratedKeyColumns(any(String.class))).thenReturn(jdbcInsert);
        da.setJdbcInsert(jdbcInsert);
        when(jdbcInsert.executeAndReturnKey(any(Map.class))).thenReturn(1);
        User user = da.save(mockUser);
        assertEquals(Long.valueOf(1), user.getId());

        when(jdbcUpdate.update(any(String.class), any(Map.class))).thenReturn(1);
        User newUser = da.save(user);
        assertEquals(Long.valueOf(1), newUser.getId());
    }

    @Test
    public void getByLogin() throws SQLException {
        ResultSet rs = mock(ResultSet.class);
        when(rs.next()).thenReturn(true);
        when(rs.getLong(any(String.class))).thenReturn(1L);
        when(rs.getString("name")).thenReturn(null);
        when(rs.getString("surname")).thenReturn(null);
        when(rs.getString("middle_name")).thenReturn(null);
        when(rs.getString("role")).thenReturn("Student");
        when(rs.getString("login")).thenReturn(null);
        when(rs.getString("password")).thenReturn(null);
        Answer<User> answer = new Answer<User>() {
            public User answer(InvocationOnMock invocationOnMock) throws Throwable {
                ResultSetExtractor<User> extractor = invocationOnMock.getArgument(1);
                User user = extractor.extractData(rs);
                return user;
            }
        };
        when(jdbcTemplate.query(any(String.class), any(ResultSetExtractor.class), any(String.class))).thenAnswer(answer);
        User user = da.getByLogin("login");
        assertEquals(mockUser.getRole(), user.getRole());
    }
}