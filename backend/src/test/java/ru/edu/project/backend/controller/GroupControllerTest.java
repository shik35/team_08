package ru.edu.project.backend.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import ru.edu.project.backend.api.groups.Group;
import ru.edu.project.backend.api.groups.GroupForm;
import ru.edu.project.backend.api.groups.GroupService;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@WebMvcTest(GroupController.class)
public class GroupControllerTest {

    public static final long GROUP_ID = 1L;
    public static final long GROUP_COURSE_ID = 1L;
    public static final String GROUP_TITLE = "TU-1";

    @Autowired
    private MockMvc mockMvc;

    @Mock
    private GroupService groupService;

    @InjectMocks
    private GroupController controller;

    private Group mockGroup =
            Group.builder()
                    .id(GROUP_ID)
                    .courseId(GROUP_COURSE_ID)
                    .title(GROUP_TITLE)
                    .started(true)
                    .build();


    public static final String GROUP_STRING = "{\"id\":" + GROUP_ID
            + ",\"courseId\":" + GROUP_COURSE_ID
            + ",\"title\":\"" + GROUP_TITLE
            + "\",\"started\":true}";
    public static final String GROUP_ARR = "[" + GROUP_STRING + "]";

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Test
    public void createGroup() throws Exception {

        when(groupService.createGroup(any(GroupForm.class))).thenReturn(mockGroup);

        String json = "{}";
        RequestBuilder builder =
                MockMvcRequestBuilders.post("/group/createGroup")
                        .accept(MediaType.APPLICATION_JSON)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(builder).andReturn();

        assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus());
        assertEquals(GROUP_STRING, result.getResponse().getContentAsString());

    }

    @Test
    public void getGroups() throws Exception {

        when(groupService.getGroups()).thenReturn(Arrays.asList(mockGroup));

        RequestBuilder builder =
                MockMvcRequestBuilders.get("/group/getGroups")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(builder).andReturn();

        assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus());
        assertEquals(GROUP_ARR, result.getResponse().getContentAsString());

    }

    @Test
    public void getByCourse() throws Exception {

        when(groupService.getByCourse(any(Long.class))).thenReturn(Arrays.asList(mockGroup));

        RequestBuilder builder =
                MockMvcRequestBuilders.get("/group/getByCourse/" + GROUP_ID)
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(builder).andReturn();
        assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus());

        assertEquals(GROUP_ARR, result.getResponse().getContentAsString());

    }

    @Test
    public void updateGroup() throws Exception {

        when(groupService.updateGroup(any(Long.class), any(GroupForm.class))).thenReturn(mockGroup);

        String json = "{}";
        RequestBuilder builder =
                MockMvcRequestBuilders.post("/group/updateGroup/" + GROUP_ID)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(builder).andReturn();

        assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus());
        assertEquals(GROUP_STRING, result.getResponse().getContentAsString());

    }

    @Test
    public void getGroup() throws Exception {

        when(groupService.getGroup(any(Long.class))).thenReturn(mockGroup);

        RequestBuilder builder =
                MockMvcRequestBuilders.get("/group/getGroup/" + GROUP_ID)
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(builder).andReturn();

        assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus());
        assertEquals(GROUP_STRING, result.getResponse().getContentAsString());

    }

    @Test
    public void deleteGroup() throws Exception {

        when(groupService.deleteGroup(any(Long.class))).thenReturn(true);

        String json = "{}";
        RequestBuilder builder =
                MockMvcRequestBuilders.post("/group/deleteGroup/" + GROUP_ID)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(builder).andReturn();

        assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus());
        assertEquals("true", result.getResponse().getContentAsString());

    }

}