package ru.edu.project.backend.da.jpa.converter;

import org.junit.Test;
import org.mapstruct.factory.Mappers;
import ru.edu.project.backend.api.courses.Course;
import ru.edu.project.backend.da.jpa.entity.CourseEntity;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class CourseMapperTest {
    private CourseMapper mapper = Mappers.getMapper(CourseMapper.class);
    private Course course = Course
            .builder()
            .id(10L)
            .title("Course title")
            .description("description")
            .teacherId(25L)
            .build();

    @Test
    public void mapToEntityNull() {
        Course nullCourse = null;
        CourseEntity entity = mapper.map(nullCourse);
        assertNull(entity);
    }

    @Test
    public void mapToEntity() {
        CourseEntity entity = mapper.map(course);
        assertEquals(entity.getTitle(), course.getTitle());
        assertEquals(entity.getId(), course.getId());
        assertEquals(entity.getDescription(), course.getDescription());
        assertEquals(entity.getTeacherId(), course.getTeacherId());
    }

    @Test
    public void mapToCourseNull() {
        CourseEntity nullCourseEntity = null;
        Course course = mapper.map(nullCourseEntity);
        assertNull(course);
    }

    @Test
    public void testToCourse() {
        CourseEntity entity = mapper.map(course);
        Course newCourse = mapper.map(entity);
        assertEquals(newCourse.getTitle(), entity.getTitle());
    }


    @Test
    public void mapListNull() {
        List<CourseEntity> entities = null;
        List<Course> courses = mapper.mapList(entities);
        assertNull(courses);
    }

    @Test
    public void mapList() {
        CourseEntity entity = mapper.map(course);
        List<CourseEntity> entities = Arrays.asList(entity);
        List<Course> courses = mapper.mapList(entities);
        assertEquals(1, courses.size());
        assertEquals(entity.getTitle(), courses.get(0).getTitle());
    }
}