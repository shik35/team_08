package ru.edu.project.backend.da.jdbcTemplate;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import ru.edu.project.backend.api.groups.Group;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GroupDATest {

    public static final long GROUP_ID = 1L;
    public static final long GROUP_COURSE_ID = 1L;
    public static final String GROUP_TITLE = "TU-1";

    @Mock
    private JdbcTemplate jdbcTemplate;

    @Mock
    private NamedParameterJdbcTemplate jdbcUpdate;

    @InjectMocks
    private GroupDA da;

    private Group mockGroup =
            Group.builder()
                    .id(GROUP_ID)
                    .courseId(GROUP_COURSE_ID)
                    .title(GROUP_TITLE)
                    .started(true)
                    .build();

    @Test
    public void insert() {
        SimpleJdbcInsert jdbcInsert = mock(SimpleJdbcInsert.class);
        when(jdbcInsert.withTableName(any(String.class))).thenReturn(jdbcInsert);
        when(jdbcInsert.usingGeneratedKeyColumns(any(String.class))).thenReturn(jdbcInsert);
        when(jdbcInsert.executeAndReturnKey(any(Map.class))).thenReturn(1L);

        da.setJdbcInsert(jdbcInsert);

        Group mockGroup =
                Group.builder()
                        .courseId(GROUP_COURSE_ID)
                        .title(GROUP_TITLE)
                        .started(true)
                        .build();

        Group group = da.save(mockGroup);
        assertEquals(GROUP_ID, group.getId().longValue());
        assertEquals(GROUP_COURSE_ID, group.getCourseId().longValue());
        assertEquals(GROUP_TITLE, group.getTitle());
        assertTrue(group.isStarted());
    }


    @Test
    public void update() {

        when(jdbcUpdate.update(
                any(String.class),
                any(Map.class))).thenReturn(1);

        Group newGroup = da.save(mockGroup);
        assertEquals(GROUP_ID, newGroup.getId().longValue());
        assertEquals(GROUP_COURSE_ID, newGroup.getCourseId().longValue());
        assertEquals(GROUP_TITLE, newGroup.getTitle());
        assertTrue(newGroup.isStarted());

    }

    @Test
    public void getById() throws SQLException {

        ResultSet rs = mock(ResultSet.class);
        when(rs.next()).thenReturn(true);
        when(rs.getLong("group_id")).thenReturn(GROUP_ID);
        when(rs.getLong("group_course_id")).thenReturn(GROUP_COURSE_ID);
        when(rs.getString("group_title")).thenReturn(GROUP_TITLE);
        when(rs.getBoolean("group_is_started")).thenReturn(true);

        Answer<Group> answer = invocationOnMock -> {
            ResultSetExtractor<Group> extractor = invocationOnMock.getArgument(1);
            return extractor.extractData(rs);
        };

        when(jdbcTemplate.query(
                any(String.class),
                any(ResultSetExtractor.class),
                any(Long.class))).thenAnswer(answer);

        Group group = da.getById(GROUP_ID);
        assertEquals(mockGroup.getId(), group.getId());
        assertEquals(mockGroup.getCourseId(), group.getCourseId());
        assertEquals(mockGroup.getTitle(), group.getTitle());
        assertEquals(mockGroup.isStarted(), group.isStarted());

    }


    @Test
    public void deleteById() {

        when(jdbcTemplate.update(
                any(String.class),
                any(Long.class))).thenReturn(1);

        Boolean result = da.deleteById(GROUP_ID);
        assertTrue(result);

    }

    @Test
    public void getGroups() throws SQLException {

        Answer<List<Group>> answer = getListAnswer();

        when(jdbcTemplate.query(
                any(String.class),
                any(RowMapper.class))).thenAnswer(answer);

        List<Group> groups = da.getGroups();
        assertEquals(1, groups.size());
        assertEquals(mockGroup.getId(), groups.get(0).getId());
        assertEquals(mockGroup.getCourseId(), groups.get(0).getCourseId());
        assertEquals(mockGroup.getTitle(), groups.get(0).getTitle());
        assertEquals(mockGroup.isStarted(), groups.get(0).isStarted());

    }

    @Test
    public void getGroupsByCourse() throws SQLException {

        Answer<List<Group>> answer = getListAnswer();

        when(jdbcTemplate.query(
                any(String.class),
                any(RowMapper.class),
                any(Long.class))).thenAnswer(answer);

        List<Group> groups = da.getGroupsByCourse(GROUP_COURSE_ID);

        assertEquals(1, groups.size());
        assertEquals(mockGroup.getId(), groups.get(0).getId());
        assertEquals(mockGroup.getCourseId(), groups.get(0).getCourseId());
        assertEquals(mockGroup.getTitle(), groups.get(0).getTitle());
        assertEquals(mockGroup.isStarted(), groups.get(0).isStarted());

    }

    private Answer<List<Group>> getListAnswer() throws SQLException {

        ResultSet rs = mock(ResultSet.class);
        when(rs.getLong("group_id")).thenReturn(GROUP_ID);
        when(rs.getLong("group_course_id")).thenReturn(GROUP_COURSE_ID);
        when(rs.getString("group_title")).thenReturn(GROUP_TITLE);
        when(rs.getBoolean("group_is_started")).thenReturn(true);

        Answer<List<Group>> answer = invocationOnMock -> {
            RowMapper<Group> mapper = invocationOnMock.getArgument(1);
            return Arrays.asList(mapper.mapRow(rs, 1));
        };

        return answer;
    }



}
