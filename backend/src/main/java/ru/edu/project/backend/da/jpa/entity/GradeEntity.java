package ru.edu.project.backend.da.jpa.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "grades")
public class GradeEntity {

    /**
     * Id оценки.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gradeIdSequence")
    @SequenceGenerator(name = "gradeIdSequence", sequenceName = "grade_id_sequence", allocationSize = 1)
    @Column(name = "grade_id")
    private Long id;

    /**
     * Id урока.
     */
    @Column(name = "grade_lesson_id")
    private String lessonId;

    /**
     * Id группы-студента.
     */
    @Column(name = "grade_group_student_id")
    private String groupStudentId;

    /**
     * Оценка.
     */
    @Column(name = "grade")
    private String grade;
}
