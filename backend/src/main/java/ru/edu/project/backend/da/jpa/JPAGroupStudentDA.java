package ru.edu.project.backend.da.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import ru.edu.project.backend.api.groups.GroupStudent;
import ru.edu.project.backend.da.GroupStudentDALayer;
import ru.edu.project.backend.da.jpa.converter.GroupStudentMapper;
import ru.edu.project.backend.da.jpa.entity.GroupStudentEntity;
import ru.edu.project.backend.da.jpa.repository.GroupStudentEntityRepository;

import java.util.List;
import java.util.Optional;

@Service
@Profile("SPRING_DATA")
public class JPAGroupStudentDA implements GroupStudentDALayer {

    /**
     * Репозиторий.
     */
    @Autowired
    private GroupStudentEntityRepository repository;

    /**
     * Мапер.
     */
    @Autowired
    private GroupStudentMapper mapper;

    /**
     * Save Group Student's info.
     *
     * @param groupStudent GroupStudent object
     * @return GroupStudent object
     */
    @Override
    public GroupStudent save(final GroupStudent groupStudent) {
        GroupStudentEntity entity = mapper.map(groupStudent);
        GroupStudentEntity saved = repository.save(entity);
        return mapper.map(saved);    }

    /**
     * Get all GroupStudent objects.
     *
     * @return list of GroupStudent objects
     */
    public List<GroupStudent> getGroupStudents() {
        return mapper.mapList(repository.findAll());
    }

    /**
     * Get all GroupStudent objects by Group id.
     *
     * @param groupId Group id
     * @return list of GroupStudent objects
     */
    public List<GroupStudent> getByGroup(final Long groupId) {
        Iterable<GroupStudentEntity> groupStudents
                = repository.findByGroupId(groupId);
        return mapper.mapList(groupStudents);
    }

    /**
     * Get Group Student by id.
     *
     * @param groupStudentId Group Student id
     * @return GroupStudent object
     */
    @Override
    public GroupStudent getById(final Long groupStudentId) {
        Optional<GroupStudentEntity> group = repository.findById(groupStudentId);
        return group.map(entity -> mapper.map(entity)).orElse(null);    }

    /**
     * Delete Group Student by Group id and User id.
     *
     * @param groupId Group id
     * @param userId  User id
     * @return Group Student was deleted?
     */
    @Override
    public Boolean delete(final Long groupId, final Long userId) {
        return false;
    }

}
