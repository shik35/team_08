package ru.edu.project.backend.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import ru.edu.project.backend.api.groups.GroupStudent;
import ru.edu.project.backend.api.groups.GroupStudentForm;
import ru.edu.project.backend.api.groups.GroupStudentService;
import ru.edu.project.backend.da.GroupStudentDALayer;

import java.util.List;

@Service
@Profile("!STUB")
public class GroupStudentServiceLayer implements GroupStudentService {

    /**
     * Logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(GroupStudentServiceLayer.class);


    /**
     * Зависимость для слоя доступа к данным заявок.
     */
    @Autowired
    private GroupStudentDALayer daLayer;


    /**
     * Creating info for Student in Group.
     *
     * @param form Data from Group's student form
     * @return new GroupStudent object
     */
    @Override
    public GroupStudent addStudentToGroup(final GroupStudentForm form) {
        GroupStudent groupStudent = GroupStudent.builder()
                .groupId(form.getGroupId())
                .userId(form.getUserId())
                .build();
        return daLayer.save(groupStudent);
    }

    /**
     * Get all GroupStudent objects.
     *
     * @return list of GroupStudent objects
     */
    @Override
    public List<GroupStudent> getGroupStudents() {
        return daLayer.getGroupStudents();
    }

    /**
     * Get all GroupStudent objects by Group id.
     *
     * @param groupId Group id
     * @return list of GroupStudent objects
     */
    @Override
    public List<GroupStudent> getByGroup(final Long groupId) {
        return daLayer.getByGroup(groupId);
    }

    /**
     * Get GroupStudent objects by id.
     *
     * @param id Group id
     * @return list of GroupStudent objects
     */
    @Override
    public GroupStudent getGroupStudent(final Long id) {
        return daLayer.getById(id);
    }

    /**
     * Delete Student from Group's students list.
     *
     * @param groupId Group id
     * @param userId  User id
     * @return updated Group object
     */
    @Override
    public boolean deleteStudentFromGroup(final Long groupId, final Long userId) {
        return daLayer.delete(groupId, userId);
    }

}
