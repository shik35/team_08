package ru.edu.project.backend.da.jdbcTemplate;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Service;
import ru.edu.project.backend.api.students.User;
import ru.edu.project.backend.da.UserDALayer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Profile("JDBC_TEMPLATE")
public class UserDA implements UserDALayer {
    /**
     * выбрать пользователя.
     */
    public static final String SELECT_ALL = "select * from users";
    /**
     * выбрать пользователя по id.
     */
    public static final String SELECT_BY_ID = "select "
            +
            "* from users where id = ?";
    /**
     * выбрать пользователя по логину.
     */
    public static final String SELECT_BY_LOGIN = "select "
            +
            "* from users where login = ?";
    /**
     * обновить данные пользователя.
     */
    public static final String UPDATE =
            "update users set name = :name, surname = :surname, "
                    +
            "middle_name = :middle_name where id = :id";

    /**
     * Зависимость на шаблон jdbc.
     */
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * Зависимость на шаблон jdbc insert.
     */
    @Autowired
    private SimpleJdbcInsert jdbcInsert;

    /**
     * Зависимость на шаблон jdbc named.
     */
    @Autowired
    private NamedParameterJdbcTemplate jdbcNamed;

    /**
     * Внедрение зависимости jdbc insert с настройкой для таблицы.
     *
     * @param bean
     */
    @Autowired
    public void setJdbcInsert(final SimpleJdbcInsert bean) {
        jdbcInsert = bean
                .withTableName("users")
                .usingGeneratedKeyColumns("id");
    }

    /**
     * Получение списка пользователей.
     *
     * @return list
     */
    @Override
    public List<User> getUsers() {
        return jdbcTemplate.query(SELECT_ALL, this::rowMapper);
    }

    /**
     * Получение пользователя по id.
     *
     * @param id
     * @return user
     */
    @Override
    public User getById(final Long id) {
        return jdbcTemplate.query(SELECT_BY_ID, this::singleRowMapper, id);
    }

    /**
     * Получение пользователя по логину.
     *
     * @param login
     * @return user
     */
    @Override
    public User getByLogin(final String login) {
        return jdbcTemplate.query(SELECT_BY_LOGIN, this::singleRowMapper, login);
    }

    /**
     * Создание или обновление юзера.
     *
     * @param user
     * @return user
     */
    @Override
    public User save(final User user) {
        if (user.getId() == null) {
            return insert(user);
        }
        return update(user);
    }

    private User update(final User user) {
        jdbcNamed.update(UPDATE, toMap(user));
        return user;
    }

    private User insert(final User user) {
        long id = jdbcInsert.executeAndReturnKey(toMap(user)).longValue();
        user.setId(id);
        return user;
    }

    private Map<String, Object> toMap(final User user) {
        HashMap<String, Object> map = new HashMap<>();
        if (user.getId() != null) {
            map.put("id", Long.valueOf(user.getId()));
        }
        map.put("name", user.getName());
        map.put("surname", user.getSurname());
        map.put("middle_name", user.getMiddleName());
        map.put("role", user.getRole().name());
        map.put("login", user.getLogin());
        map.put("password", user.getPassword());
        return map;
    }


    @SneakyThrows
    private User rowMapper(final ResultSet rs, final int pos) {
        return mapRow(rs);
    }

    @SneakyThrows
    private User singleRowMapper(final ResultSet rs) {
        rs.next();
        return mapRow(rs);
    }

    private User mapRow(final ResultSet rs) throws SQLException {
        return User.builder().
                id(rs.getLong("id")).
                name(rs.getString("name")).
                surname(rs.getString("surname")).
                middleName(rs.getString("middle_name")).
                role(User.UserRole.valueOf(rs.getString("role"))).
                login(rs.getString("login")).
                password(rs.getString("password")).
                build();
    }
}
