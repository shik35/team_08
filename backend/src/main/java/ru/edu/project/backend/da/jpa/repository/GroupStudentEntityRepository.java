package ru.edu.project.backend.da.jpa.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import ru.edu.project.backend.da.jpa.entity.GroupStudentEntity;

@Repository
public interface GroupStudentEntityRepository extends
        PagingAndSortingRepository<GroupStudentEntity, Long>,
        JpaSpecificationExecutor<GroupStudentEntity> {

    /**
     *
     * @param groupId
     * @return GroupStudentEntity object
     */
    Iterable<GroupStudentEntity> findByGroupId(Long groupId);

    /**
     *
     * @param userId
     * @return GroupStudentEntity object
     */
    Iterable<GroupStudentEntity> findByUserId(Long userId);


}
