package ru.edu.project.backend.da.jpa.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import ru.edu.project.backend.da.jpa.entity.GradeEntity;
import ru.edu.project.backend.da.jpa.entity.UserEntity;

@Repository
public interface GradeEntityRepository
        extends PagingAndSortingRepository<GradeEntity, Long>, JpaSpecificationExecutor<UserEntity> {
}

