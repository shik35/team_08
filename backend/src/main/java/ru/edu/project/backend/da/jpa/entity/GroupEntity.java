package ru.edu.project.backend.da.jpa.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "groups")
public class GroupEntity {

    /**
     * Group's id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator = "groups_id_sequence")
    @SequenceGenerator(name = "groups_id_sequence",
            sequenceName = "groups_id_sequence",
            allocationSize = 1)
    @Column(name = "group_id")
    private Long id;

    /**
     * Group's course id.
     */
    @Column(name = "group_course_id")
    private Long courseId;

    /**
     * Group's title.
     */
    @Column(name = "group_title")
    private String title;

    /**
     * Is group started their education.
     */
    @Column(name = "group_is_started")
    private boolean started;


}
