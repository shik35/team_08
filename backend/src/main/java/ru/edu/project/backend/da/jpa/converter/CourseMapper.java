package ru.edu.project.backend.da.jpa.converter;

import org.mapstruct.Mapper;
import ru.edu.project.backend.api.courses.Course;
import ru.edu.project.backend.da.jpa.entity.CourseEntity;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CourseMapper {

    /**
     * @param course
     * @return Course
     */
    Course map(CourseEntity course);

    /**
     * @param course
     * @return CourseEntity
     */
    CourseEntity map(Course course);

    /**
     * @param courses
     * @return List<Course>
     */
    List<Course> mapList(Iterable<CourseEntity> courses);
}
