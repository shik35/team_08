package ru.edu.project.backend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import ru.edu.project.backend.api.groups.GroupStudent;
import ru.edu.project.backend.api.groups.GroupStudentForm;
import ru.edu.project.backend.api.groups.GroupStudentService;


import java.util.List;

@RestController
@RequestMapping("/groupStudent/")
public class GroupStudentController implements GroupStudentService {

    /**
     * Делегат для передачи вызова.
     */
    @Autowired
    private GroupStudentService delegate;

    /**
     * Creating info for Student in Group.
     *
     * @param form Data from Group's student form
     * @return new GroupStudent object
     */
    @Override
    @PostMapping("/addStudentToGroup")
    public GroupStudent addStudentToGroup(
            @RequestBody final GroupStudentForm form) {
        return delegate.addStudentToGroup(form);
    }

    /**
     * Get all GroupStudent objects.
     *
     * @return list of GroupStudent objects
     */
    @Override
    @GetMapping("/getGroupStudents")
    public List<GroupStudent> getGroupStudents() {
        return delegate.getGroupStudents();
    }

    /**
     * Get all GroupStudent objects by Group id.
     *
     * @param groupId Group id
     * @return list of GroupStudent objects
     */
    @Override
    @GetMapping("/getByGroup/{groupId}")
    public List<GroupStudent> getByGroup(@PathVariable("groupId") final Long groupId) {
        return delegate.getByGroup(groupId);
    }

    /**
     * Get GroupStudent objects by id.
     *
     * @param id Group id
     * @return list of GroupStudent objects
     */
    @Override
    @GetMapping("/getGroupStudent/{id}")
    public GroupStudent getGroupStudent(@PathVariable("id") final Long id) {
        return delegate.getGroupStudent(id);
    }

    /**
     * Delete Student from Group's students list.
     *
     * @param groupId Group id
     * @param userId  User id
     * @return updated Group object
     */
    @Override
    @PostMapping("/deleteStudentFromGroup")
    public boolean deleteStudentFromGroup(@RequestParam(value = "groupId") final Long groupId,
                                          @RequestParam(value = "userId") final Long userId) {
        return delegate.deleteStudentFromGroup(groupId, userId);
    }

}
