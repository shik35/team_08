package ru.edu.project.backend.da.jpa.entity;

import lombok.Getter;
import lombok.Setter;
import ru.edu.project.backend.api.students.User;
import ru.edu.project.backend.api.students.UserAbstract;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "users")
public class UserEntity implements UserAbstract {
    /**
     * id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
    generator = "users_id_sequence")
    @SequenceGenerator(name = "users_id_sequence",
            sequenceName = "users_id_sequence",
            allocationSize = 1)
    private Long id;

    /**
     * name.
     */
    @Column(name = "name")
    private String name;

    /**
     * surname.
     */
    @Column(name = "surname")
    private String surname;

    /**
     * middle name.
     */
    @Column(name = "middle_name")
    private String middleName;

    /**
     * role.
     */

    @Column(name = "role")
    @Enumerated(EnumType.STRING)
    private User.UserRole role;

    /**
     * login.
     */
    @Column(name = "login")
    private String login;

    /**
     * password.
     */
    @Column(name = "password")
    private String password;
}
