package ru.edu.project.backend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
//import ru.edu.project.backend.api.courses.Course;
import ru.edu.project.backend.api.grades.Grade;
import ru.edu.project.backend.api.grades.GradeForm;
import ru.edu.project.backend.api.grades.GradeService;

import java.util.List;

@RestController
@RequestMapping("/grade")
public class GradeController implements GradeService {

    /**
     * Делегат для передачи вызова.
     */
    @Autowired
    private GradeService delegate;

    /**
     * создание оценки через .builder.
     *
     * @param form
     * @return new grade
     */
    @Override
    @PostMapping("/createGrade")
    public Grade createGrade(@RequestBody final GradeForm form) {
        return delegate.createGrade(form);
    }

    /**
     * @return list
     */
    @Override
    @GetMapping("/getGrades")
    public List<Grade> getGrades() {
        return delegate.getGrades();
    }

    /**
     * @param form
     * @return updated
     */
    @Override
    @PostMapping("/saveGrade")
    public Grade saveGrade(@RequestBody final GradeForm form) {
        return delegate.saveGrade(form);
    }

    /**
     * Get Grade by id.
     *
     * @param id Grade id
     * @return Grade object
     */
    @Override
    @GetMapping("/getGrade/{id}")
    public Grade getGrade(@PathVariable("id") final Long id) {
        return delegate.getGrade(id);
    }

    /**
     * Delete Grade by id.
     *
     * @param id Grade id
     * @return Grade was deleted?
     */
    @Override
    @GetMapping("/deleteGrade/{id}")
    public Boolean deleteGrade(@PathVariable("id") final Long id) {
        return delegate.deleteGrade(id);
    }

    /**
     * Delete All Grades.
     *
     * @return All grades were deleted?
     */
    @Override
    @GetMapping("/deleteGrades")
    public Boolean deleteGrades() {
        return delegate.deleteGrades();
    }

}
