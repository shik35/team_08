package ru.edu.project.backend.da.jpa.converter;

import org.mapstruct.Mapper;
import ru.edu.project.backend.api.grades.Grade;
import ru.edu.project.backend.da.jpa.entity.GradeEntity;

import java.util.List;

@Mapper(componentModel = "spring")
public interface GradeMapper {

    /**
     * @param grade
     * @return Grade
     */
    Grade map(GradeEntity grade);

    /**
     * @param grade
     * @return GradeEntity
     */
    GradeEntity map(Grade grade);

    /**
     * @param grades
     * @return List<Grade>
     */
    List<Grade> mapList(Iterable<GradeEntity> grades);
}

