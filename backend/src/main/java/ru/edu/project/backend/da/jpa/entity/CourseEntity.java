package ru.edu.project.backend.da.jpa.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "courses")
public class CourseEntity {

    /**
     * Id кусса.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "courseIdSequence")
    @SequenceGenerator(name = "courseIdSequence", sequenceName = "course_id_sequence", allocationSize = 1)
    @Column(name = "id")
    private Long id;

    /**
     * Наименование курса.
     */
    @Column(name = "title")
    private String title;

    /**
     * Описание курса.
     */
    @Column(name = "course_description")
    private String description;

    /**
     * Id преподавателя курса.
     */
    @Column(name = "course_teacher_user_id")
    private Long teacherId;
}
