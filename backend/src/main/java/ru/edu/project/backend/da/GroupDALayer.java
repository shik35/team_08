package ru.edu.project.backend.da;

import ru.edu.project.backend.api.groups.Group;

import java.util.List;

public interface GroupDALayer {

    /**
     * Save Group's info.
     * @param group Group object
     * @return Group object
     */
    Group save(Group group);

    /**
     * Get Group by id.
     * @param id Group id
     * @return Group object
     */
    Group getById(Long id);

    /**
     * Delete Group by id.
     * @param id Group id
     * @return Group was deleted?
     */
    Boolean deleteById(Long id);

    /**
     * Get all Groups from database.
     * @return List of Group objects
     */
    List<Group> getGroups();

    /**
     * Get Groups by Course id.
     * @param courseId Course id
     * @return List of Group objects
     */
    List<Group> getGroupsByCourse(Long courseId);


}
