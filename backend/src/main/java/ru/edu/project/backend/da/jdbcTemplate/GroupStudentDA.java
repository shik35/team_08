package ru.edu.project.backend.da.jdbcTemplate;

import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Service;
import ru.edu.project.backend.api.groups.GroupStudent;
import ru.edu.project.backend.da.GroupStudentDALayer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Profile("JDBC_TEMPLATE")
public class GroupStudentDA implements GroupStudentDALayer {

    /**
     * Logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(GroupDA.class);

    /**
     * Выбрать все записи.
     */
    public static final String SQL_SELECT_ALL =
            "SELECT * FROM group_students";

    /**
     * Удалить записи.
     */
    public static final String SQL_DELETE_ALL =
            "DELETE FROM group_students";

    /**
     * Выбрать по id.
     */
    public static final String SQL_WHERE_ID =
            " WHERE group_student_id = ?";

    /**
     * Выбрать записи по Group id.
     */
    public static final String SQL_WHERE_GROUP_ID =
            " WHERE group_student_group_id = ?";

    /**
     * Выбрать записи по Group id и User id.
     */
    public static final String SQL_WHERE_GROUP_ID_AND_USER_ID =
            " WHERE group_student_group_id = ?"
                    + " AND group_student_user_id = ?";

    /**
     * Зависимость на шаблон jdbc.
     */
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * Зависимость на шаблон jdbc insert.
     */
    @Autowired
    private SimpleJdbcInsert jdbcInsert;

    /**
     * Зависимость на шаблон jdbc named.
     */
    @Autowired
    private NamedParameterJdbcTemplate jdbcNamed;

    /**
     * Внедрение зависимости jdbc insert с настройкой для таблицы.
     *
     * @param bean SimpleJdbcInsert Bean
     */
    @Autowired
    public void setJdbcInsert(final SimpleJdbcInsert bean) {
        jdbcInsert = bean
                .withTableName("group_students")
                .usingGeneratedKeyColumns("group_student_id");
    }

    /**
     * Save Group Student's info.
     *
     * @param groupStudent GroupStudent object
     * @return GroupStudent object
     */
    @Override
    public GroupStudent save(final GroupStudent groupStudent) {
        Long id = jdbcInsert.executeAndReturnKey(toMap(groupStudent)).longValue();
        groupStudent.setId(id);
        return groupStudent;
    }

    /**
     * Get all Groups Students from database.
     *
     * @return List of GroupStudents objects
     */
    @Override
    public List<GroupStudent> getGroupStudents() {
        return jdbcTemplate.query(SQL_SELECT_ALL,
                this::rowMapper);
    }

    /**
     * Get Group Students by Group id.
     *
     * @param groupId Group id
     * @return List of GroupStudents object
     */
    @Override
    public List<GroupStudent> getByGroup(final Long groupId) {
        return jdbcTemplate.query(
                SQL_SELECT_ALL + SQL_WHERE_GROUP_ID,
                this::rowMapper,
                groupId);
    }

    /**
     * Get Group Student by id.
     *
     * @param groupStudentId Group Student id
     * @return GroupStudent object
     */
    @Override
    public GroupStudent getById(final Long groupStudentId) {
        return jdbcTemplate.query(SQL_SELECT_ALL + SQL_WHERE_ID,
                this::singleRowMapper,
                groupStudentId);
    }

    /**
     * Delete Group Student by Group id and User id.
     *
     * @param groupId Group id
     * @param userId  User id
     * @return Group Student was deleted?
     */
    @Override
    public Boolean delete(final Long groupId, final Long userId) {
        int result = jdbcTemplate.update(
                SQL_DELETE_ALL + SQL_WHERE_GROUP_ID_AND_USER_ID,
                groupId,
                userId);
        return result == 1;
    }

    private Map<String, Object> toMap(final GroupStudent groupStudent) {
        HashMap<String, Object> map = new HashMap<>();

        map.put("group_student_group_id", groupStudent.getGroupId());
        map.put("group_student_user_id", groupStudent.getUserId());

        return map;
    }

    @SneakyThrows
    private GroupStudent rowMapper(final ResultSet rs,
                                   final int pos) {
        return mapRow(rs);
    }

    @SneakyThrows
    private GroupStudent singleRowMapper(final ResultSet rs) {
        if (rs.next()) {
            return mapRow(rs);
        } else {
            return null;
        }
    }

    private GroupStudent mapRow(final ResultSet rs)
            throws SQLException {
        return GroupStudent.builder()
                .id(rs.getLong("group_student_id"))
                .groupId(rs.getLong("group_student_group_id"))
                .userId(rs.getLong("group_student_user_id"))
                .build();
    }

}
