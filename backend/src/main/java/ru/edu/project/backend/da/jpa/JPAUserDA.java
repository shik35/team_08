package ru.edu.project.backend.da.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import ru.edu.project.backend.api.students.User;
import ru.edu.project.backend.da.UserDALayer;
import ru.edu.project.backend.da.jpa.converter.UserMapper;
import ru.edu.project.backend.da.jpa.entity.UserEntity;
import ru.edu.project.backend.da.jpa.repository.UserEntityRepository;

import java.util.List;
import java.util.Optional;

@Service
@Profile("SPRING_DATA")
public class JPAUserDA implements UserDALayer {
    /**
     * repository.
     */
    @Autowired
    private UserEntityRepository repo;

    /**
     * mapper.
     */
    @Autowired
    private UserMapper mapper;

    /**
     * Получение списка пользователей.
     *
     * @return list
     */
    @Override
    public List<User> getUsers() {
        return mapper.mapList(repo.findAll());
    }

    /**
     * Получение пользователя по id.
     *
     * @param id
     * @return user
     */
    @Override
    public User getById(final Long id) {
        Optional<UserEntity> user = repo.findById(id);
        return user.map(entity -> mapper.map(entity)).orElse(null);
    }

    /**
     * Создание или обновление юзера.
     *
     * @param user
     * @return user
     */
    @Override
    public User save(final User user) {
        UserEntity entity = mapper.map(user);
        UserEntity saved = repo.save(entity);
        return mapper.map(saved);
    }

    /**
     * Получение пользователя по логину.
     *
     * @param login
     * @return user
     */
    @Override
    public User getByLogin(final String login) {
        Optional<UserEntity> user = repo.findByLogin(login);
        if (!user.isPresent()) {
            throw new IllegalArgumentException("No such login");
        }
        return user.map(entity -> mapper.map(entity)).orElse(null);
    }
}
