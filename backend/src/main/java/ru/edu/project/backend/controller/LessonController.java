package ru.edu.project.backend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.edu.project.backend.api.lessons.Lesson;
import ru.edu.project.backend.api.lessons.LessonForm;
import ru.edu.project.backend.api.lessons.LessonService;

import java.util.List;

@RestController
@RequestMapping("/lesson")
public class LessonController implements LessonService {

    /**
     * Делегат для передачи вызова.
     */
    @Autowired
    private LessonService delegate;

    /**
     * Создание урока.
     *
     * @param form - LessonForm
     * @return Lesson.
     */
    @Override
    @PostMapping("/createLesson")
    public Lesson createLesson(@RequestBody final LessonForm form) {
        return delegate.createLesson(form);
    }

    /**
     * Сохранение существующего урока после редактирования.
     *
     * @param form - LessonForm
     * @return Lesson.
     */
    @Override
    @PostMapping("/saveLesson")
    public Lesson saveLesson(@RequestBody final LessonForm form) {
        return delegate.saveLesson(form);
    }

    /**
     * Получить список уроков.
     *
     * @return List<Lesson>
     */
    @Override
    @GetMapping(value = "/getLessons")
    public List<Lesson> getLessons() {
        return delegate.getLessons();
    }

    /**
     * Получить урок по id.
     *
     * @param id id
     * @return List<Lesson>
     */
    @Override
    @GetMapping("/getLesson/{id}")
    public Lesson getLesson(@PathVariable("id") final Long id) {
        return delegate.getLesson(id);
    }
}
