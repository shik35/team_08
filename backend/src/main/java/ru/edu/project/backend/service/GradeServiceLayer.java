package ru.edu.project.backend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import ru.edu.project.backend.api.grades.Grade;
import ru.edu.project.backend.api.grades.GradeForm;
import ru.edu.project.backend.api.grades.GradeService;
import ru.edu.project.backend.da.GradeDALayer;

import java.util.List;

@Service
@Profile("!STUB")
public class GradeServiceLayer implements GradeService {

    /**
     * Зависимость для слоя доступа к данным оценок.
     */
    @Autowired
    private GradeDALayer daLayer;

    /**
     * создание оценки через .builder.
     *
     * @param form
     * @return new grade
     */
    @Override
    public Grade createGrade(final GradeForm form) {
        System.out.println("_________CREATE_GRADE");
        Grade newGrade = Grade.builder().
                lessonId(form.getLessonId()).
                groupStudentId(form.getGroupStudentId()).
                grade(form.getGrade()).build();
        System.out.println("_________CREATE_GRADE_grade: " + newGrade.toString());
        return daLayer.save(newGrade);
    }

    /**
     * @return list
     */
    @Override
    public List<Grade> getGrades() {
        return daLayer.getGrades();
    }

    /**
     * Сохранение существующего курса после редактирования.
     *
     * @param form - GradeForm
     * @return Grade.
     */
    @Override
    public Grade saveGrade(final GradeForm form) {
        /*
        System.out.println("_______________IN_GRADE_SERVICE_LAYER");
        System.out.println("id: " + form.getId());
        System.out.println("lesson_id: " + form.getLessonId());
        System.out.println("group_sudent_id: " + form.getGroupStudentId());
        System.out.println("grade: " + form.getGrade());
         */
        Grade grade = Grade.builder().
                id(form.getId()).
                lessonId(form.getLessonId()).
                groupStudentId(form.getGroupStudentId()).
                grade(form.getGrade()).build();
        return daLayer.save(grade);
    }

    /**
     * Get Grade by id.
     *
     * @param id Grade id
     * @return Grade object
     */
    @Override
    public Grade getGrade(final Long id) {
        return daLayer.getById(id);
    }

    /**
     * Delete Grade by id.
     *
     * @param id Grade id
     * @return Grade was deleted?
     */
    @Override
    public Boolean deleteGrade(final Long id) {
        //System.out.println("_______________IN_GRADE_SERVICE_LAYER__GRADE_DELETE");
        //System.out.println("_______________id:" + id);
        return daLayer.deleteById(id);
    }

    /**
     * Delete All Grades.
     *
     * @return All grades were deleted?
     */
    @Override
    public Boolean deleteGrades() {
        //System.out.println("_______________IN_GRADE_SERVICE_LAYER__GRADE_DELETE__ALL");
        return daLayer.deleteAll();
    }
}
