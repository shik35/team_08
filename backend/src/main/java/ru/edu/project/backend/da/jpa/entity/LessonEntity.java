package ru.edu.project.backend.da.jpa.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "lessons")
public class LessonEntity {

    /**
     * Id урока.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "lessons_id_sequence")
    @SequenceGenerator(name = "lessons_id_sequence", sequenceName = "lessons_id_sequence", allocationSize = 1)
    @Column(name = "id")
    private Long id;

    /**
     * Наименование урока.
     */
    @Column(name = "lesson_title")
    private String title;

    /**
     * Описание урока.
     */
    @Column(name = "lesson_description")
    private String description;

    /**
     * Домашнее задание к уроку.
     */
    @Column(name = "lesson_homework")
    private String homework;

    /**
     * Идентификатор курса.
     */
    @Column(name = "lesson_course_id")
    private Long courseId;

}
