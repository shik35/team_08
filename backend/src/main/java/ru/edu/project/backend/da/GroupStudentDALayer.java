package ru.edu.project.backend.da;

import ru.edu.project.backend.api.groups.GroupStudent;

import java.util.List;

public interface GroupStudentDALayer {

    /**
     * Save Group Student's info.
     *
     * @param groupStudent GroupStudent object
     * @return GroupStudent object
     */
    GroupStudent save(GroupStudent groupStudent);

    /**
     * Get all Groups Students from database.
     *
     * @return List of GroupStudents objects
     */
    List<GroupStudent> getGroupStudents();

    /**
     * Get Group Students by Group id.
     *
     * @param groupId Group id
     * @return List of GroupStudents object
     */
    List<GroupStudent> getByGroup(Long groupId);

    /**
     * Get Group Student by id.
     *
     * @param groupStudentId Group Student id
     * @return GroupStudent object
     */
    GroupStudent getById(Long groupStudentId);

    /**
     * Delete Group Student by Group id and User id.
     *
     * @param groupId Group id
     * @param userId  User id
     * @return Group Student was deleted?
     */
    Boolean delete(Long groupId, Long userId);

}
