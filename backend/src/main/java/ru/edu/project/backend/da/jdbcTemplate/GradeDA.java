package ru.edu.project.backend.da.jdbcTemplate;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Service;
import ru.edu.project.backend.api.grades.Grade;
import ru.edu.project.backend.da.GradeDALayer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Profile("JDBC_TEMPLATE")
public class GradeDA implements GradeDALayer {

    /**
     * выбрать все оценки.
     */
    public static final String SELECT_ALL = "SELECT * FROM `grades`";

    /**
     * выбрать оценку по id.
     */
    public static final String SELECT_BY_ID = "SELECT * "
            + "FROM `grades` "
            + "WHERE `grade_id` = ?";

    /**
     * обновить данные оценки.
     */
    public static final String UPDATE = "UPDATE `grades` "
            + "SET `grade` = :grade, `grade_lesson_id` = :lessonId, "
            + "`grade_group_student_id` = :groupStudentId "
            + " WHERE `grade_id` = :id";

    /**
     * удалить все оценки.
     */
    public static final String SQL_DELETE_ALL =
            "DELETE FROM `grades`";

    /**
     * выбрать оценку по id.
     */
    public static final String SQL_DELETE_BY_ID =
            "DELETE FROM `grades` WHERE `grade_id` = ?";

    /**
     * Зависимость на шаблон jdbc.
     */
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * Зависимость на шаблон jdbc insert.
     */
    @Autowired
    private SimpleJdbcInsert jdbcInsert;

    /**
     * Зависимость на шаблон jdbc named.
     */
    @Autowired
    private NamedParameterJdbcTemplate jdbcNamed;

    /**
     * Внедрение зависимости jdbc insert с настройкой для таблицы.
     *
     * @param bean
     */
    @Autowired
    public void setJdbcInsert(final SimpleJdbcInsert bean) {
        jdbcInsert = bean
                .withTableName("grades")
                .usingGeneratedKeyColumns("grade_id");
    }

    /**
     * Получение списка оценок.
     *
     * @return list
     */
    @Override
    public List<Grade> getGrades() {
        return jdbcTemplate.query(SELECT_ALL, this::rowMapper);
    }

    /**
     * Получение оценки по id.
     *
     * @param id
     * @return grade
     */
    @Override
    public Grade getById(final Long id) {
        return jdbcTemplate.query(SELECT_BY_ID, this::singleRowMapper, id);
    }

    /**
     * Создание или обновление оценки.
     *
     * @param grade
     * @return grade
     */
    @Override
    public Grade save(final Grade grade) {
        if (grade.getId() == null) {
            return insert(grade);
        }
        return update(grade);
    }

    /**
     * Delete Grade by id.
     *
     * @param id Grade id
     * @return Grade was deleted?
     */
    @Override
    public Boolean deleteById(final Long id) {
        int result = jdbcTemplate.update(SQL_DELETE_BY_ID, id);
        return result == 1;
    }

    /**
     * Delete All Grades.
     *
     * @return All grades deleted?
     */
    @Override
    public Boolean deleteAll() {
        //System.out.println("_________________GRADE_DA_DELETE");
        int result = jdbcTemplate.update(SQL_DELETE_ALL);
        //System.out.println("_________________GRADE_DA_DELETE_result: " + result);
        return result > 0;
    }

    private Grade update(final Grade grade) {
        /*
        System.out.println("_________________GRADE_DA");
        System.out.println("_______GRADE_id:" + grade.getId());
        System.out.println("_______GRADE_lesson_id:" + grade.getLessonId());
        System.out.println("_______GRADE_group_student_id:" + grade.getGroupStudentId());
        System.out.println("_______GRADE_grade:" + grade.getGrade());
         */
        //jdbcNamed.update(UPDATE, toMap(grade)); // что-то не так с мапой или шаблоном
        String sql = "UPDATE `grades` "
                + "SET `grade`=" + grade.getGrade() + ", "
                + "`grade_lesson_id`=" + grade.getLessonId() + ", "
                + "`grade_group_student_id`=" + grade.getGroupStudentId()
                + " WHERE `grade_id`=" + grade.getId() + ";";
        //System.out.println("___________GRADE_sql:" + sql);
        jdbcNamed.update(sql, toMap(grade));
        return grade;
    }

    private Grade insert(final Grade grade) {
        //System.out.println("________INSERT_GRADE:" + grade.getLessonId());
        Long id = jdbcInsert.executeAndReturnKey(toMap(grade)).longValue();
        //System.out.println("________INSERT_GRADE_id: " + id);
        grade.setId(id);
        return grade;
    }

    private Map<String, Object> toMap(final Grade grade) {
        HashMap<String, Object> map = new HashMap<>();
        if (grade.getId() != null) {
            map.put("grade_id", grade.getId());
        }
        map.put("grade_lesson_id", grade.getLessonId());
        map.put("grade_group_student_id", grade.getGroupStudentId());
        map.put("grade", grade.getGrade());
        //System.out.println("_______________");
        //System.out.println("_______________MAP" + map.toString());
        return map;
    }

    @SneakyThrows
    private Grade rowMapper(final ResultSet rs, final int pos) {
        return mapRow(rs);
    }

    @SneakyThrows
    private Grade singleRowMapper(final ResultSet rs) {
        rs.next();
        return mapRow(rs);
    }

    private Grade mapRow(final ResultSet rs) throws SQLException {
        return Grade.builder().
                id(rs.getLong("grade_id")).
                lessonId(rs.getString("grade_lesson_id")).
                groupStudentId(rs.getString("grade_group_student_id")).
                grade(rs.getString("grade")).
                build();
    }
}
