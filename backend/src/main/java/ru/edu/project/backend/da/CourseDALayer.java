package ru.edu.project.backend.da;

import ru.edu.project.backend.api.courses.Course;

import java.util.List;

public interface CourseDALayer {

    /**
     * Получение списка курсов.
     *
     * @return list
     */
    List<Course> getCourses();

    /**
     * Получение курса по id.
     *
     * @param id
     * @return Course
     */
    Course getById(Long id);

    /**
     * Создание или обновление курса.
     *
     * @param course
     * @return Course
     */
    Course save(Course course);
}
