package ru.edu.project.stub.lessons;

import ru.edu.project.backend.api.lessons.Lesson;
import ru.edu.project.backend.api.lessons.LessonForm;
import ru.edu.project.backend.api.lessons.LessonService;

import java.util.List;

public class LessonInMemoryService implements LessonService {

    /**
     * Создание урока.
     *
     * @param form - LessonForm
     * @return Lesson.
     */
    @Override
    public Lesson createLesson(final LessonForm form) {
        return null;
    }

    /**
     * Сохранение существующего урока после редактирования.
     *
     * @param form - LessonForm
     * @return Lesson.
     */
    @Override
    public Lesson saveLesson(final LessonForm form) {
        return null;
    }

    /**
     * Получить список уроков.
     *
     * @return List<Lesson>
     */
    @Override
    public List<Lesson> getLessons() {
        return null;
    }

    /**
     * Получить урок по id.
     *
     * @param id id
     * @return List<Lesson>
     */
    @Override
    public Lesson getLesson(final Long id) {
        return null;
    }
}
