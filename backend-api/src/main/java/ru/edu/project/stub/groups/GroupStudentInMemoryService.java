package ru.edu.project.stub.groups;

import ru.edu.project.backend.api.groups.GroupStudent;
import ru.edu.project.backend.api.groups.GroupStudentForm;
import ru.edu.project.backend.api.groups.GroupStudentService;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class GroupStudentInMemoryService implements GroupStudentService {

    /**
     * Счетчик для идентификаторов групп.
     */
    private Long idCounter = 1L;

    /**
     * storage.
     */
    private final ConcurrentHashMap<Long, GroupStudent> groupStudents = new ConcurrentHashMap<>();

    /**
     * Creating info for Student in Group.
     *
     * @param form Data from Group's student form
     * @return new GroupStudent object
     */
    @Override
    public GroupStudent addStudentToGroup(final GroupStudentForm form) {
        if (form == null) {
            throw new IllegalArgumentException("Form is null");
        }

        GroupStudent result = GroupStudent.builder()
                .id(idCounter++)
                .groupId(form.getGroupId())
                .userId(form.getUserId())
                .build();

        groupStudents.put(result.getId(), result);
        return result;
    }

    /**
     * Get all GroupStudent objects.
     *
     * @return list of GroupStudent objects
     */
    @Override
    public List<GroupStudent> getGroupStudents() {
        return new ArrayList<>(groupStudents.values());
    }

    /**
     * Get all GroupStudent objects by Group id.
     *
     * @param groupId Group id
     * @return list of GroupStudent objects
     */
    @Override
    public List<GroupStudent> getByGroup(final Long groupId) {
        return groupStudents
                .values()
                .stream()
                .filter(group -> group.getGroupId().equals(groupId))
                .collect(Collectors.toList());
    }

    /**
     * Get GroupStudent objects by id.
     *
     * @param id Group id
     * @return list of GroupStudent objects
     */
    @Override
    public GroupStudent getGroupStudent(final Long id) {
        return groupStudents.get(id);
    }

    /**
     * Delete Student from Group's students list.
     *
     * @param groupId Group id
     * @param userId  User id
     * @return updated Group object
     */
    @Override
    public boolean deleteStudentFromGroup(final Long groupId, final Long userId) {

        List<GroupStudent> studentsToRemove;
        studentsToRemove = groupStudents.values().stream()
                .filter(groupSt -> groupSt
                        .getGroupId()
                        .equals(groupId))
                .filter(groupSt -> groupSt
                        .getUserId()
                        .equals(userId))
                .collect(Collectors.toList());

        if (studentsToRemove.size() > 0) {
            groupStudents.remove(studentsToRemove.get(0).getId());
            return true;
        } else {
            return false;
        }

    }
}
