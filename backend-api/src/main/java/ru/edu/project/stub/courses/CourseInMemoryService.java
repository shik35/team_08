package ru.edu.project.stub.courses;

import lombok.NonNull;
import ru.edu.project.backend.api.courses.Course;
import ru.edu.project.backend.api.courses.CourseForm;
import ru.edu.project.backend.api.courses.CourseService;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class CourseInMemoryService implements CourseService {

    /**
     * Счетчик для идентификаторов курсов.
     */
    private Long courseId = 0L;

    /**
     * Хранилище курсов в памяти.
     */
    private ConcurrentHashMap<Long, Course> courses = new ConcurrentHashMap<>();

    /**
     * Создание курса.
     *
     * @param form - CourseForm
     * @return Course.
     */
    @Override
    public Course createCourse(final @NonNull CourseForm form) {
        if (form.getTitle() == null) {
            throw new IllegalArgumentException("invalid form");
        }
        if (courses.containsKey(form.getTitle())) {
            throw new IllegalArgumentException("This Course has already been created");
        }
        Long id = courseId++;
        Course course = Course
                .builder()
                .id(id)
                .title(form.getTitle())
                .description(form.getDescription())
                .teacherId(form.getTeacherId())
                .build();
        courses.put(id, course);
        return course;
    }

    /**
     * Сохранение существующего курса после редактирования.
     *
     * @param form - CourseForm
     * @return Course.
     */
    @Override
    public Course saveCourse(final CourseForm form) {
        Course course = null;
        if (courses.containsKey(form.getId())) {
            course = Course
                    .builder()
                    .id(form.getId())
                    .title(form.getTitle())
                    .description(form.getDescription())
                    .teacherId(form.getTeacherId())
                    .build();
            courses.put(form.getId(), course);
        }
        return course;
    }

    /**
     * Получить список курсов.
     *
     * @return List<Course>
     */
    @Override
    public List<Course> getCourses() {
        return new LinkedList<>(courses.values());
    }

    /**
     * Получить курс gj id.
     *
     * @param id id
     * @return List<Course>
     */
    @Override
    public Course getCourse(final Long id) {
        return courses.get(id);
    }
}
