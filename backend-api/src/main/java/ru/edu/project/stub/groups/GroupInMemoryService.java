package ru.edu.project.stub.groups;

import ru.edu.project.backend.api.groups.Group;
import ru.edu.project.backend.api.groups.GroupForm;
import ru.edu.project.backend.api.groups.GroupService;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class GroupInMemoryService implements GroupService {


    /**
     * Счетчик для идентификаторов групп.
     */
    private Long idCounter = 1L;

    /**
     * Map of all groups.
     **/
    private final ConcurrentHashMap<Long, Group> groups = new ConcurrentHashMap<>();

    /**
     * Creating group.
     *
     * @param form Data from Group form
     * @return new Group object
     */
    @Override
    public Group createGroup(final GroupForm form) {
        if (form == null) {
            throw new IllegalArgumentException("Form is null");
        }

        Group result = Group.builder()
                .id(idCounter++)
                .courseId(form.getCourseId())
                .title(form.getTitle())
                .started(form.isStarted())
                .build();

        groups.put(result.getId(), result);
        return result;
    }

    /**
     * Get all groups from database.
     *
     * @return list of Group objects
     */
    @Override
    public List<Group> getGroups() {
        return new ArrayList<>(groups.values());
    }

    /**
     * Get groups list by course id.
     *
     * @param courseId Course Id
     * @return list of Group objects
     */
    @Override
    public List<Group> getByCourse(final Long courseId) {
        return groups
                .values()
                .stream()
                .filter(group -> group.getCourseId().equals(courseId))
                .collect(Collectors.toList());

    }

    /**
     * Update Group info.
     *
     * @param id   Group id
     * @param form Data from Group form
     * @return updated Group object
     */
    @Override
    public Group updateGroup(final Long id, final GroupForm form) {

        Group group = groups.get(id);
        if (group == null) {
            throw new IllegalArgumentException("Group's Id not found.");
        }

        group.setCourseId(form.getCourseId());
        group.setTitle(form.getTitle());
        group.setStarted(form.isStarted());

        groups.put(group.getId(), group);
        return group;

    }

    /**
     * Update Group info.
     *
     * @param form Data from Group form
     * @return updated Group object
     */
    @Override
    public Group updateGroup(final GroupForm form) {
        return updateGroup(form.getId(), form);
    }

    /**
     * Get Group's info.
     *
     * @param id Group id
     * @return Group object
     */
    @Override
    public Group getGroup(final Long id) {
        return groups.get(id);
    }

    /**
     * Delete Group by id.
     *
     * @param id Group id
     * @return Group was deleted?
     */
    @Override
    public boolean deleteGroup(final Long id) {

        if (groups.get(id) != null) {
            groups.remove(id);
            return true;
        } else {
            return false;
        }

    }
}
