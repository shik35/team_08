package ru.edu.project.backend.api.grades;

import ru.edu.project.backend.api.common.AcceptorArgument;

import java.util.List;

public interface GradeService {
    /**
     * создание оценки через .builder.
     * @param form
     * @return new grade
     */
    @AcceptorArgument
    Grade createGrade(GradeForm form);

    /**
     *
     * @return list
     */
    List<Grade> getGrades();

    /**
     * Сохранение существующей оценки после редактирования.
     *
     * @param form - GradeForm
     * @return Grade.
     */
    @AcceptorArgument
    Grade saveGrade(GradeForm form);

    /**
     * Get Grade by id.
     * @param id Grade id
     * @return Grade object
     */
    Grade getGrade(Long id);

    /**
     * Delete Grade by id.
     * @param id Grade id
     * @return Grade was deleted?
     */
    //@AcceptorArgument  // это нужно только для POST!!!
    Boolean deleteGrade(Long id);

    /**
     * Delete All Grades.
     *
     * @return All grades were deleted?
     */
    Boolean deleteGrades();

}
