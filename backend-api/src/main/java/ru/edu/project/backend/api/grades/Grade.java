package ru.edu.project.backend.api.grades;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.jackson.Jacksonized;

@Getter
@Setter
@Jacksonized
@Builder
public class Grade {

    /**
     * grade id.
     */
    private Long id;

    /**
     * lesson id.
     */
    private String lessonId;

    /**
     * groupStudent id.
     */
    private String groupStudentId;

    /**
     * grade.
     */
    private String grade;

}
