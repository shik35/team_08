package ru.edu.project.backend.api.courses;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.jackson.Jacksonized;

@Getter
@Setter
@Builder
@Jacksonized
@ToString
@EqualsAndHashCode
public class Course {

    /**
     * Id курса.
     */
    private Long id;

    /**
     * Наименование курса.
     */
    private String title;

    /**
     * Описание курса.
     */
    private String description;

    /**
     * Id преподавателя.
     */
    private Long teacherId;
}
