package ru.edu.project.backend.api.courses;

import ru.edu.project.backend.api.common.AcceptorArgument;

import java.util.List;

public interface CourseService {

    /**
     * Создание курса.
     *
     * @param form - CourseForm
     * @return Course.
     */
    @AcceptorArgument
    Course createCourse(CourseForm form);

    /**
     * Сохранение существующего курса после редактирования.
     *
     * @param form - CourseForm
     * @return Course.
     */
    @AcceptorArgument
    Course saveCourse(CourseForm form);

    /**
     * Получить список курсов.
     *
     * @return List<Course>
     */
    List<Course> getCourses();

    /**
     * Получить курс по id.
     *
     * @param id id
     * @return List<Course>
     */
    Course getCourse(Long id);
}
