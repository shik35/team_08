package ru.edu.project.backend.api.lessons;

import ru.edu.project.backend.api.common.AcceptorArgument;

import java.util.List;

public interface LessonService {

    /**
     * Создание урока.
     *
     * @param form - LessonForm
     * @return Lesson.
     */
    @AcceptorArgument
    Lesson createLesson(LessonForm form);

    /**
     * Сохранение существующего урока после редактирования.
     *
     * @param form - LessonForm
     * @return Lesson.
     */
    @AcceptorArgument
    Lesson saveLesson(LessonForm form);

    /**
     * Получить список уроков.
     *
     * @return List<Lesson>
     */
    List<Lesson> getLessons();

    /**
     * Получить урок по id.
     *
     * @param id id
     * @return List<Lesson>
     */
    Lesson getLesson(Long id);
}
