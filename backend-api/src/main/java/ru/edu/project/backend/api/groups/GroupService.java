package ru.edu.project.backend.api.groups;

import ru.edu.project.backend.api.common.AcceptorArgument;

import java.util.List;

public interface GroupService {

    /**
     * Creating group.
     * @param form Data from Group form
     * @return new Group object
     */
    @AcceptorArgument
    Group createGroup(GroupForm form);

    /**
     * Get all groups from database.
     * @return list of Group objects
     */
    List<Group> getGroups();

    /**
     * Get groups list by course id.
     * @param courseId Course Id
     * @return list of Group objects
     */
    List<Group> getByCourse(Long courseId);

    /**
     * Update Group info.
     * @param id Group id
     * @param form Data from Group form
     * @return updated Group object
     */
    @AcceptorArgument
    Group updateGroup(Long id, GroupForm form);

    /**
     * Update Group info.
     * @param form Data from Group form
     * @return updated Group object
     */
    @AcceptorArgument
    Group updateGroup(GroupForm form);

    /**
     * Get Group's info.
     * @param id Group id
     * @return Group object
     */
    Group getGroup(Long id);


    /**
     * Delete Group by id.
     * @param id Group id
     * @return Group was deleted?
     */
    @AcceptorArgument
    boolean deleteGroup(Long id);

}
