package ru.edu.project.backend.api.groups;

import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

@Getter
@Jacksonized
@Builder
public class GroupForm {

    /**
     * Group id.
     */
    private Long id;


    /**
     * Group's course id.
     */
    private Long courseId;

    /**
     * Group's title.
     */
    private String title;

    /**
     * Is group started their education.
     */
    private boolean started;

}
