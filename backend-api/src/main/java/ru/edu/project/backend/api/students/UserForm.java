package ru.edu.project.backend.api.students;

import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

@Getter
@Jacksonized
@Builder
public class UserForm {
    /**
     * имя.
     */
    private String name;
    /**
     * фамилия.
     */
    private String surname;
    /**
     * отчество.
     */
    private String middleName;

    /**
     * логин.
     */
    private String login;

    /**
     * пароль.
     */
    private String password;

    /**
     * роль пользователя.
     */
    private User.UserRole role;

}
