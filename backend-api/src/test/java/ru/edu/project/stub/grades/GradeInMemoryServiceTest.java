package ru.edu.project.stub.grades;

import org.junit.Test;
import java.util.List;

import ru.edu.project.backend.api.grades.Grade;
import ru.edu.project.backend.api.grades.GradeForm;
import ru.edu.project.backend.api.grades.GradeService;

import static org.junit.Assert.*;
import static org.junit.Assert.assertFalse;

public class GradeInMemoryServiceTest {
    public static final Long NEW_GRADE_ID = 2L;
    public static final String NEW_LESSON_ID = "2";
    public static final String NEW_GROUP_STUDENT_ID = "2";
    public static final String NEW_GRADE = "3";

    GradeService service = new GradeInMemoryService();

    Grade grade = service
            .createGrade(GradeForm
                    .builder()
                    .lessonId("1")
                    .groupStudentId("1")
                    .grade("5")
                    .build());

    GradeForm gradeForm = GradeForm
            .builder()
            .id(NEW_GRADE_ID)
            .lessonId(NEW_LESSON_ID)
            .groupStudentId(NEW_GROUP_STUDENT_ID)
            .grade(NEW_GRADE)
            .build();
    Grade gradeFromForm = service.createGrade(gradeForm);

    @Test
    public void classGradeTest() {
        assertNotNull(gradeFromForm.getId());
        assertEquals(NEW_LESSON_ID, gradeFromForm.getLessonId());
        assertEquals(NEW_GROUP_STUDENT_ID, gradeFromForm.getGroupStudentId());
        assertEquals(NEW_GRADE, gradeFromForm.getGrade());
    }

    @Test
    public void createGrade() {
        assertEquals("5", grade.getGrade());
        assertNotNull(grade.getId());
    }

    @Test(expected = IllegalArgumentException.class)
    public void createAlreadyExistedGrade()
    {
        service.createGrade(GradeForm.builder().lessonId("1").groupStudentId("1").grade("5").build());
    }

    @Test
    public void deleteGrade() {
        assertTrue(service.deleteGrade(grade.getId()));
        assertFalse(service.deleteGrade(grade.getId()));
    }

    @Test
    public void deleteGrades() {
        GradeService serviceNew = new GradeInMemoryService();
        serviceNew.createGrade(GradeForm
                        .builder()
                        .lessonId("3")
                        .groupStudentId("2")
                        .grade("1")
                        .build());
        serviceNew.createGrade(GradeForm
                .builder()
                .lessonId("4")
                .groupStudentId("5")
                .grade("4")
                .build());

        assertTrue(serviceNew.deleteGrades());
        assertFalse(serviceNew.deleteGrades());
    }

    @Test
    public void updateGrade() {
        GradeForm gradeFormNew = GradeForm
                .builder()
                .id(50L)
                .lessonId("3")
                .groupStudentId(NEW_GROUP_STUDENT_ID)
                .grade(NEW_GRADE)
                .build();
        Grade testGrade = service.createGrade(gradeFormNew);
        Long gradeId = testGrade.getId();

        GradeForm gradeFormUpdated = GradeForm
                .builder()
                .id(gradeId)
                .lessonId("3")
                .groupStudentId("1")
                .grade("3")
                .build();
        Grade updatedGrade = service.saveGrade(gradeFormUpdated);

        assertEquals("3", updatedGrade.getLessonId());
        assertEquals("1", updatedGrade.getGroupStudentId());
        assertEquals("3", updatedGrade.getGrade());
    }

    @Test
    public void getGrade() {
        GradeService serviceNew = new GradeInMemoryService();
        GradeForm gradeFormNew = GradeForm
                .builder()
                .id(10L)
                .lessonId("3")
                .groupStudentId(NEW_GROUP_STUDENT_ID)
                .grade(NEW_GRADE)
                .build();
        Grade testGrade = serviceNew.createGrade(gradeFormNew);

        Long gradeId = testGrade.getId();
        List<Grade> gradesNew = serviceNew.getGrades();
        //gradesNew.getGrade(gradeId);

        assertEquals( 1, gradesNew.size());
        assertEquals("3", gradesNew.get(0).getGrade());
        assertEquals(gradesNew.toString(), "[" + gradesNew.get(0).toString() + "]");
    }

    @Test(expected = IllegalArgumentException.class)
    public void nullForm() {
        GradeForm gradeFormNew = null;
        Grade testGradeNew = service.createGrade(gradeFormNew);
    }

}
