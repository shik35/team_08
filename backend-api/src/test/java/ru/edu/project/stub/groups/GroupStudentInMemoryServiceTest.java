package ru.edu.project.stub.groups;

import org.junit.Test;
import ru.edu.project.backend.api.groups.*;

import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;
import static org.junit.Assert.assertFalse;

public class GroupStudentInMemoryServiceTest {


    public static final Long GROUP_ID = 1L;
    public static final Long USER_ID = 1L;
    public static final Long NEW_USER_ID = 2L;
    public static final Long NEW_GROUP_ID = 2L;
    GroupStudentService service = new GroupStudentInMemoryService();
    GroupStudentForm groupStudentForm = GroupStudentForm
            .builder()
            .groupId(GROUP_ID)
            .userId(USER_ID)
            .build();
    GroupStudent groupStudent = service.addStudentToGroup(
            groupStudentForm);

    @Test
    public void classGroupTest() {
        groupStudent.setId(1L);
        groupStudent.setUserId(NEW_USER_ID);
        groupStudent.setGroupId(NEW_GROUP_ID);

        assertEquals(NEW_GROUP_ID, groupStudent.getGroupId());
        assertEquals(NEW_USER_ID, groupStudent.getUserId());
    }


    @Test
    public void addStudentToGroup() {
        assertEquals(GROUP_ID, groupStudent.getGroupId());
        assertEquals(USER_ID, groupStudent.getUserId());
        assertNotNull(groupStudent.getId());
//
//        System.out.println(groupStudent);
//        System.out.println(groupStudentForm);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createGroupWithException() {
        service.addStudentToGroup(null);
    }

    @Test
    public void getGroupStudents() {
        List<GroupStudent> groupStudents;
        groupStudents = service.getGroupStudents();
        assertEquals(1, groupStudents.size());
    }

    @Test
    public void getGroupStudentsByGroup() {
        List<GroupStudent> groupStudents;
        groupStudents = service.getByGroup(GROUP_ID);
        assertEquals(1, groupStudents.size());
    }

    @Test
    public void getGroupStudent() {
        GroupStudent newGroupStudent = service.getGroupStudent(groupStudent.getId());
        assertEquals(GROUP_ID, newGroupStudent.getGroupId());
        assertEquals(USER_ID, newGroupStudent.getUserId());
    }

    @Test
    public void deleteStudentFromGroup() {
        assertTrue(service.deleteStudentFromGroup(
                groupStudent.getGroupId(),
                groupStudent.getUserId()));
        assertFalse(service.deleteStudentFromGroup(
                groupStudent.getGroupId(),
                groupStudent.getUserId()));

    }

}
